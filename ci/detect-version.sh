#!/bin/bash
set -xeuo pipefail
export TZ=Etc/Utc
branch_name="${CI_COMMIT_REF_NAME:-$(git rev-parse --abbrev-ref HEAD)}"

# NOTE: the output of this script MUST NOT contain a trailing newline; it is
# used without stripping by tools.

# WARNING: when changing the output of this script, make sure to update the
# matchers in the tag-release job to prevent the development or alpha versions
# from being published.

function unique_ref() {
    prerelease="$1"
    printf '%s.g%s' "$(date "+0.%Y%m%d.0-$prerelease.T%H%M%S")" "$(git rev-parse --short HEAD)"
}

function stable_ref() {
    base="$(date "+0.%Y%m%d.")"
    existing_tags="$(git tag -l "$base*" | wc -l)"
    # is this evil?
    printf '%s%s' "$base" "$existing_tags"
}

if [[ "$branch_name" == 'devel' ]]; then
    unique_ref alpha
elif [[ "$branch_name" == 'rolling' ]]; then
    unique_ref rolling
elif [[ "$branch_name" == 'stable' ]]; then
    stable_ref
else
    printf "0.0.0-dev-${CI_COMMIT_REF_SLUG}"
fi
