#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import copy
import unittest
import unittest.mock
import uuid

import yaook.statemachine.api_utils as api_utils
import yaook.statemachine.context as context


class TestContext(unittest.TestCase):
    def setUp(self):
        self.intf = unittest.mock.Mock([])
        self.intf.plural = "testobjects"
        self.ctx = context.Context(
            namespace="test-namespace",
            parent={
                "apiVersion": "test.yaook.cloud/v1",
                "metadata": {
                    "name": "test-object",
                    "uid": str(uuid.uuid4()),
                },
                "spec": unittest.mock.sentinel.parent_spec,
            },
            parent_intf=self.intf,
            instance=None,
            instance_data=None,
            api_client=unittest.mock.sentinel.api_client,
            logger=unittest.mock.sentinel.logger,
            field_manager=unittest.mock.sentinel.field_manager,
        )

    def test_base_label_match_without_instance(self):
        self.assertEqual(
            {
                context.LABEL_PARENT_PLURAL: "testobjects",
                context.LABEL_PARENT_GROUP: "test.yaook.cloud",
                context.LABEL_PARENT_VERSION: "v1",
                context.LABEL_PARENT_NAME: "test-object",
            },
            self.ctx.base_label_match(),
        )

    def test_with_instance_returns_copy_when_no_instance_is_set(self):
        instanced = self.ctx.with_instance("foobar")
        self.assertIsNot(instanced, self.ctx)
        self.assertIsNone(self.ctx.instance)

        self.assertEqual(instanced.instance, "foobar")

    def test_with_instance_raises_if_instance_is_set(self):
        instanced = self.ctx.with_instance("foobar")

        with self.assertRaisesRegex(ValueError, "already has an instance"):
            instanced.with_instance("baz")

    def test_without_instance_returns_context_without_instance(self):
        instanced = self.ctx.with_instance("foobar")
        reverted = instanced.without_instance()
        self.assertIsNone(reverted.instance)

    def test_without_instance_is_idempotent(self):
        self.assertIsNone(self.ctx.without_instance().instance)
        instanced = self.ctx.with_instance("foobar")
        instanced.without_instance().without_instance()

    def test_base_label_match_with_instance(self):
        label_match = self.ctx.with_instance("foobar").base_label_match()

        self.assertIn(context.LABEL_INSTANCE, label_match)
        self.assertEqual(label_match[context.LABEL_INSTANCE], "foobar")

    def test_with_instance_fails_if_name_too_long(self):
        # 64 chars
        instance = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"  # noqa: E501
        with self.assertRaisesRegex(
                ValueError,
                "Instance is longer than 63 characters and k8s api calls wont"
                f" work. Instance: {instance}, Length: {len(instance)}"):
            self.ctx.with_instance(instance)


class Testrecover_parent_reference(unittest.TestCase):
    def setUp(self):
        self.metadata = {
            "labels": {
                context.LABEL_PARENT_GROUP:
                    unittest.mock.sentinel.parent_group,
                context.LABEL_PARENT_PLURAL:
                    unittest.mock.sentinel.parent_plural,
                context.LABEL_PARENT_VERSION:
                    unittest.mock.sentinel.parent_version,
                context.LABEL_PARENT_NAME:
                    unittest.mock.sentinel.parent_name,
                context.LABEL_INSTANCE:
                    unittest.mock.sentinel.instance,
            },
            "namespace": unittest.mock.sentinel.namespace,
        }

    def test_extracts_labels_and_namespace_from_object(self):
        ref, instance = context.recover_parent_reference(
            self.metadata,
        )

        self.assertIsInstance(ref, api_utils.ResourceReference)
        self.assertEqual(
            ref.api_version,
            "{}/{}".format(
                unittest.mock.sentinel.parent_group,
                unittest.mock.sentinel.parent_version,
            ),
        )
        self.assertEqual(
            ref.plural,
            unittest.mock.sentinel.parent_plural,
        )
        self.assertEqual(
            ref.namespace,
            unittest.mock.sentinel.namespace,
        )
        self.assertEqual(
            ref.name,
            unittest.mock.sentinel.parent_name,
        )
        self.assertEqual(
            instance,
            unittest.mock.sentinel.instance,
        )

    def test_accepts_missing_instance(self):
        del self.metadata["labels"][context.LABEL_INSTANCE]

        ref, instance = context.recover_parent_reference(
            self.metadata,
        )

        self.assertIsNone(instance)

    def test_raises_if_critical_label_is_missing(self):
        critical_labels = [
            context.LABEL_PARENT_GROUP,
            context.LABEL_PARENT_PLURAL,
            context.LABEL_PARENT_VERSION,
            context.LABEL_PARENT_NAME,
        ]

        for label in critical_labels:
            new_metadata = copy.deepcopy(self.metadata)
            del new_metadata["labels"][label]

            with self.assertRaisesRegex(
                    ValueError,
                    "does not belong to the statemachine"):
                context.recover_parent_reference(
                    new_metadata,
                )
