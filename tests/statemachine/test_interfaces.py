#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import copy
import itertools
import json
import unittest
import unittest.mock
import uuid

import ddt
import kubernetes_asyncio.client

import yaook.statemachine.interfaces as interfaces


class Testget_kubernetes_version(unittest.IsolatedAsyncioTestCase):
    async def test_gets_version(self):
        with contextlib.ExitStack() as stack:
            get_code = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.VersionApi.get_code",
                new=unittest.mock.AsyncMock()
            ))
            get_code.return_value = unittest.mock.sentinel.k8sversion

            result = await interfaces.get_kubernetes_version(None)

        self.assertEqual(
            unittest.mock.sentinel.k8sversion,
            result
        )


class Testgenerate_name(unittest.TestCase):
    def test_passthrough_if_name_set(self):
        body = {
            "metadata": {
                "name": "test123"
            },
            "spec": unittest.mock.sentinel.spec
        }
        bodycopy = copy.deepcopy(body)
        interfaces.generate_name(body)
        self.assertEqual(body, bodycopy)

    @unittest.mock.patch("yaook.statemachine.interfaces.rng.choice")
    def test_generates_name(self, choice):
        body = {
            "metadata": {
                "generateName": "test123-"
            },
            "spec": unittest.mock.sentinel.spec
        }
        choice.side_effect = ["a", "b", "c", "d", "e"]
        interfaces.generate_name(body)
        self.assertEqual(body["metadata"]["name"], "test123-abcde")

    @unittest.mock.patch("yaook.statemachine.interfaces.rng.choice")
    def test_generates_name_too_long(self, choice):
        body = {
            "metadata": {
                "generateName": "xxxxxx-this-string-is-too-long-and-will-be-truncated-here-this-will-be-removed"  # noqa: E501
            },
            "spec": unittest.mock.sentinel.spec
        }
        choice.side_effect = ["a", "b", "c", "d", "e"]
        interfaces.generate_name(body)
        self.assertEqual(
            body["metadata"]["name"],
            "xxxxxx-this-string-is-too-long-and-will-be-truncated-here-abcde")

    def test_raises_if_name_and_generatename_missing(self):
        body = {
            "metadata": {
            },
            "spec": unittest.mock.sentinel.spec
        }
        self.assertRaises(ValueError, interfaces.generate_name, body)


class Teststandard_list(unittest.IsolatedAsyncioTestCase):
    async def test_extracts_k8s_items_from_object(self):
        obj = unittest.mock.Mock()
        obj.items = [
            unittest.mock.sentinel.item1,
            unittest.mock.sentinel.item2,
            unittest.mock.sentinel.item3,
        ]

        @interfaces.standard_list
        async def fetch():
            return obj

        self.assertSequenceEqual(
            await fetch(),
            [
                unittest.mock.sentinel.item1,
                unittest.mock.sentinel.item2,
                unittest.mock.sentinel.item3,
            ],
        )

    async def test_forwards_all_arguments(self):
        fetch = unittest.mock.AsyncMock()
        fetch.return_value = unittest.mock.Mock()
        fetch.return_value.items = [
            unittest.mock.sentinel.item1,
            unittest.mock.sentinel.item2,
            unittest.mock.sentinel.item3,
        ]

        wrapped = interfaces.standard_list(fetch)

        self.assertEqual(
            await wrapped(unittest.mock.sentinel.arg1,
                          kw=unittest.mock.sentinel.arg2),
            [
                unittest.mock.sentinel.item1,
                unittest.mock.sentinel.item2,
                unittest.mock.sentinel.item3,
            ],
        )

        fetch.assert_called_once_with(
            unittest.mock.sentinel.arg1,
            kw=unittest.mock.sentinel.arg2,
        )

    async def test_extracts_resource_version_from_object(self):
        obj = unittest.mock.Mock()
        obj.items = [
            unittest.mock.sentinel.item1,
            unittest.mock.sentinel.item2,
            unittest.mock.sentinel.item3,
        ]

        @interfaces.standard_list
        async def fetch():
            return obj

        result = await fetch()

        self.assertEqual(
            result.resource_version,
            obj.metadata.resource_version,
        )

        self.assertIsInstance(result, interfaces.ListWrapper)


class Testemulated_delete_collection(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.listfn = unittest.mock.AsyncMock([])
        self.deletefn = unittest.mock.AsyncMock([])
        self.edc = interfaces.emulated_delete_collection(
            self.listfn,
            self.deletefn,
        )

    async def test_lists_and_deletes(self):
        items = [
            unittest.mock.sentinel.item1,
            unittest.mock.sentinel.item2,
            unittest.mock.sentinel.item3,
        ]
        self.listfn.return_value = list(items)

        def generate_metadata():
            for i in itertools.count():
                yield {
                    "namespace": getattr(unittest.mock.sentinel,
                                         f"namespace{i}"),
                    "name": getattr(unittest.mock.sentinel, f"name{i}"),
                }

        with contextlib.ExitStack() as stack:
            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = generate_metadata()

            await self.edc(
                unittest.mock.sentinel.namespace,
                label_selector=unittest.mock.sentinel.label_selector,
            )

        self.listfn.assert_awaited_once_with(
            unittest.mock.sentinel.namespace,
            label_selector=unittest.mock.sentinel.label_selector,
        )

        self.assertCountEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call(item)
                for item in self.listfn.return_value
            ],
        )

        self.assertCountEqual(
            self.deletefn.await_args_list,
            [
                unittest.mock.call(
                    unittest.mock.sentinel.name0,
                    unittest.mock.sentinel.namespace0,
                ),
                unittest.mock.call(
                    unittest.mock.sentinel.name1,
                    unittest.mock.sentinel.namespace1,
                ),
                unittest.mock.call(
                    unittest.mock.sentinel.name2,
                    unittest.mock.sentinel.namespace2,
                ),
            ],
        )


@ddt.ddt()
class TestResourceInterface(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.createfn = unittest.mock.AsyncMock()
        self.readfn = unittest.mock.AsyncMock()
        self.listfn = unittest.mock.AsyncMock()
        self.patchfn = unittest.mock.AsyncMock()
        self.deletefn = unittest.mock.AsyncMock()
        self.replacefn = unittest.mock.AsyncMock()
        self.deletecollfn = unittest.mock.AsyncMock()
        self.ri = interfaces.ResourceInterface(
            "test.yaook.cloud/v1", "testobjects",
            createfn=self.createfn,
            readfn=self.readfn,
            listfn=self.listfn,
            patchfn=self.patchfn,
            replacefn=self.replacefn,
            deletefn=self.deletefn,
            deletecollfn=self.deletecollfn,
        )

    def test_group_set(self):
        self.assertEqual(self.ri.group, "test.yaook.cloud")

    def test_version_set(self):
        self.assertEqual(self.ri.version, "v1")

    @unittest.mock.patch("yaook.statemachine.interfaces.generate_name")
    async def test_create(self, generate_name):
        self.listfn.return_value = interfaces.ListWrapper(
            init=[],
            resource_version=str(unittest.mock.sentinel.resource_version))
        self.patchfn.return_value = unittest.mock.sentinel.patch_result

        result = await self.ri.create(
            unittest.mock.sentinel.namespace,
            {
                "metadata": {
                    "name": "test123"
                },
                "spec": str(unittest.mock.sentinel.spec)
            },
            unittest.mock.sentinel.field_manager,
        )

        generate_name.assert_called_once()

        self.listfn.assert_called_once_with(
            unittest.mock.sentinel.namespace,
            field_selector="metadata.name=test123"
        )

        self.patchfn.assert_called_once_with(
            "test123",
            unittest.mock.sentinel.namespace,
            b'{"metadata": {"name": "test123", "resourceVersion": "sentinel.resource_version"}, "spec": "sentinel.spec"}',  # noqa: E501
            field_manager=unittest.mock.sentinel.field_manager,
            force=True,
        )

        self.assertEqual(
            result,
            unittest.mock.sentinel.patch_result,
        )

    @unittest.mock.patch("yaook.statemachine.interfaces.generate_name")
    async def test_create_fail_if_static_name_in_use(self, generate_name):
        self.listfn.return_value = interfaces.ListWrapper(
            init=[{"metadata": {"name": "test123"}}],
            resource_version=str(unittest.mock.sentinel.resource_version))
        self.patchfn.return_value = unittest.mock.sentinel.patch_result

        with self.assertRaises(kubernetes_asyncio.client.ApiException) as e:
            await self.ri.create(
                unittest.mock.sentinel.namespace,
                {
                    "metadata": {
                        "name": "test123"
                    },
                    "spec": str(unittest.mock.sentinel.spec)
                },
                unittest.mock.sentinel.field_manager,
            )

            self.assertEqual(409, e.status)
            self.assertIn("already in use", e.reason)

        generate_name.assert_called_once()

        self.listfn.assert_called_once_with(
            unittest.mock.sentinel.namespace,
            field_selector="metadata.name=test123"
        )

    @unittest.mock.patch("yaook.statemachine.interfaces.generate_name")
    async def test_create_regenerates_name_on_duplicate(self, generate_name):
        self.listfn.side_effect = [
            interfaces.ListWrapper(
                init=[{"metadata": {"name": "test123-suffix1"}}],
                resource_version=str(unittest.mock.sentinel.resource_version)),
            interfaces.ListWrapper(
                init=[],
                resource_version=str(unittest.mock.sentinel.resource_version)),
        ]
        self.patchfn.return_value = unittest.mock.sentinel.patch_result

        def generate_name_mock(body):
            body["metadata"]["name"] = \
                f"test123-suffix{generate_name.call_count}"

        generate_name.side_effect = generate_name_mock

        result = await self.ri.create(
            unittest.mock.sentinel.namespace,
            {
                "metadata": {
                    "generateName": "test123-"
                },
                "spec": str(unittest.mock.sentinel.spec)
            },
            unittest.mock.sentinel.field_manager,
        )

        self.assertEqual(2, generate_name.call_count)

        self.listfn.assert_has_calls([
            unittest.mock.call(
                unittest.mock.sentinel.namespace,
                field_selector="metadata.name=test123-suffix1"
            ),
            unittest.mock.call(
                unittest.mock.sentinel.namespace,
                field_selector="metadata.name=test123-suffix2"
            )
        ])

        self.patchfn.assert_called_once_with(
            "test123-suffix2",
            unittest.mock.sentinel.namespace,
            b'{"metadata": {"generateName": "test123-", "name": "test123-suffix2", "resourceVersion": "sentinel.resource_version"}, "spec": "sentinel.spec"}',  # noqa: E501
            field_manager=unittest.mock.sentinel.field_manager,
            force=True,
        )

        self.assertEqual(
            result,
            unittest.mock.sentinel.patch_result,
        )

    @unittest.mock.patch("yaook.statemachine.interfaces.generate_name")
    async def test_create_fail_if_name_alway_conflicts(self, generate_name):
        self.listfn.return_value = interfaces.ListWrapper(
            init=[{"metadata": {"name": "test123"}}],
            resource_version=str(unittest.mock.sentinel.resource_version))
        self.patchfn.return_value = unittest.mock.sentinel.patch_result

        def generate_name_mock(body):
            body["metadata"]["name"] = "test123"

        generate_name.side_effect = generate_name_mock

        with self.assertRaises(Exception) as e:
            await self.ri.create(
                unittest.mock.sentinel.namespace,
                {
                    "metadata": {
                        "generateName": "test123"
                    },
                    "spec": str(unittest.mock.sentinel.spec)
                },
                unittest.mock.sentinel.field_manager,
            )

            self.assertIn("unable to generate a unique name", str(e.exception))

        self.assertEqual(10, generate_name.call_count)

        for call in self.listfn.call_args_list:
            self.assertEqual(call, unittest.mock.call(
                unittest.mock.sentinel.namespace,
                field_selector="metadata.name=test123"))

        self.patchfn.assert_not_called()

    @unittest.mock.patch("yaook.statemachine.interfaces.generate_name")
    async def test_create_retries_if_list_changes(self, generate_name):
        self.listfn.side_effect = [
            interfaces.ListWrapper(
                init=[],
                resource_version="1"
            ),
            interfaces.ListWrapper(
                init=[],
                resource_version="2"
            ),
        ]
        self.patchfn.side_effect = [
            kubernetes_asyncio.client.ApiException(status=409),
            unittest.mock.sentinel.patch_result
        ]

        result = await self.ri.create(
            unittest.mock.sentinel.namespace,
            {
                "metadata": {
                    "name": "test123"
                },
                "spec": str(unittest.mock.sentinel.spec)
            },
            unittest.mock.sentinel.field_manager,
        )

        self.assertEqual(2, generate_name.call_count)

        for call in self.listfn.call_args_list:
            self.assertEqual(call, unittest.mock.call(
                unittest.mock.sentinel.namespace,
                field_selector="metadata.name=test123"))

        self.assertEqual(
            self.patchfn.call_args_list,
            [
                unittest.mock.call(
                    "test123",
                    unittest.mock.sentinel.namespace,
                    b'{"metadata": {"name": "test123", "resourceVersion": "1"}, "spec": "sentinel.spec"}',  # noqa: E501
                    field_manager=unittest.mock.sentinel.field_manager,
                    force=True,
                ),
                unittest.mock.call(
                    "test123",
                    unittest.mock.sentinel.namespace,
                    b'{"metadata": {"name": "test123", "resourceVersion": "2"}, "spec": "sentinel.spec"}',  # noqa: E501
                    field_manager=unittest.mock.sentinel.field_manager,
                    force=True,
                )
            ]
        )

        self.assertEqual(
            result,
            unittest.mock.sentinel.patch_result,
        )

    async def test_read(self):
        self.readfn.return_value = unittest.mock.sentinel.read_result

        result = await self.ri.read(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
        )

        self.readfn.assert_called_once_with(
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.namespace,
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.read_result,
        )

    async def test_list(self):
        self.listfn.return_value = unittest.mock.sentinel.list_result

        result = await self.ri.list_(
            unittest.mock.sentinel.namespace,
        )

        self.listfn.assert_called_once_with(
            unittest.mock.sentinel.namespace,
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.list_result,
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.build_selector")
    async def test_list_with_label_match_dict(self, build_selector):
        self.listfn.return_value = unittest.mock.sentinel.list_result
        build_selector.return_value = unittest.mock.sentinel.selector

        result = await self.ri.list_(
            unittest.mock.sentinel.namespace,
            label_selector=unittest.mock.sentinel.selector_input,
        )

        build_selector.assert_called_once_with(
            unittest.mock.sentinel.selector_input,
        )

        self.listfn.assert_called_once_with(
            unittest.mock.sentinel.namespace,
            label_selector=unittest.mock.sentinel.selector,
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.list_result,
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.build_selector")
    async def test_list_with_field_match_dict(self, build_selector):
        self.listfn.return_value = unittest.mock.sentinel.list_result
        build_selector.return_value = unittest.mock.sentinel.selector

        result = await self.ri.list_(
            unittest.mock.sentinel.namespace,
            field_selector=unittest.mock.sentinel.selector_input,
        )

        build_selector.assert_called_once_with(
            unittest.mock.sentinel.selector_input,
        )

        self.listfn.assert_called_once_with(
            unittest.mock.sentinel.namespace,
            field_selector=unittest.mock.sentinel.selector,
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.list_result,
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.build_selector")
    async def test_list_with_label_and_field_match_dict(self, build_selector):
        self.listfn.return_value = unittest.mock.sentinel.list_result
        build_selector.side_effect = [
            unittest.mock.sentinel.label_selector,
            unittest.mock.sentinel.field_selector,
        ]

        result = await self.ri.list_(
            unittest.mock.sentinel.namespace,
            label_selector=unittest.mock.sentinel.label_selector_input,
            field_selector=unittest.mock.sentinel.field_selector_input,
        )

        build_selector.assert_has_calls([
            unittest.mock.call(unittest.mock.sentinel.label_selector_input),
            unittest.mock.call(unittest.mock.sentinel.field_selector_input),
        ], any_order=True)

        self.listfn.assert_called_once_with(
            unittest.mock.sentinel.namespace,
            label_selector=unittest.mock.sentinel.label_selector,
            field_selector=unittest.mock.sentinel.field_selector,
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.list_result,
        )

    async def test_patch(self):
        self.patchfn.return_value = unittest.mock.sentinel.patch_result

        result = await self.ri.patch(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.patch,
        )

        self.patchfn.assert_called_once_with(
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.patch,
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.patch_result,
        )

    async def test_patch_forwards_kwargs(self):
        self.patchfn.return_value = unittest.mock.sentinel.patch_result

        result = await self.ri.patch(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.patch,
            foo="bar",
        )

        self.patchfn.assert_called_once_with(
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.patch,
            foo="bar",
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.patch_result,
        )

    async def test_replace(self):
        self.replacefn.return_value = unittest.mock.sentinel.replace_result

        result = await self.ri.replace(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.body,
        )

        self.replacefn.assert_called_once_with(
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.body,
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.replace_result,
        )

    async def test_delete(self):
        await self.ri.delete(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
        )

        self.deletefn.assert_called_once_with(
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.namespace,
            propagation_policy="Background"
        )

    @ddt.data(
        (interfaces.DeletionPropagationPolicy.BACKGROUND, "Background"),
        (interfaces.DeletionPropagationPolicy.FOREGROUND, "Foreground"))
    async def test_delete_propagation_policy(self, data):
        await self.ri.delete(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
            propagation_policy=data[0]
        )

        self.deletefn.assert_called_once_with(
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.namespace,
            propagation_policy=data[1]
        )

    async def test_delete_collection(self):
        await self.ri.delete_collection(
            unittest.mock.sentinel.namespace,
            label_selector=unittest.mock.sentinel.selector,
        )

        self.deletecollfn.assert_called_once_with(
            unittest.mock.sentinel.namespace,
            label_selector=unittest.mock.sentinel.selector,
        )

    async def test_emulates_delete_collection_if_missing(self):
        emulator = unittest.mock.AsyncMock()

        with contextlib.ExitStack() as stack:
            emulated_delete_collection = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.interfaces.emulated_delete_collection",
                )
            )
            emulated_delete_collection.return_value = emulator

            ri = interfaces.ResourceInterface(
                "test.yaook.cloud/v1", "testobjects",
                createfn=self.createfn,
                readfn=self.readfn,
                listfn=self.listfn,
                patchfn=self.patchfn,
                replacefn=self.replacefn,
                deletefn=self.deletefn,
            )

        emulated_delete_collection.assert_called_once_with(
            self.listfn,
            self.deletefn,
        )

        await ri.delete_collection(
            unittest.mock.sentinel.namespace,
            label_selector=unittest.mock.sentinel.selector,
        )

        emulator.assert_awaited_once_with(
            unittest.mock.sentinel.namespace,
            label_selector=unittest.mock.sentinel.selector,
        )

    @ddt.data(
        {},
        {"metadata": {}},
        {"metadata": {"managedFields": []}},
        {"metadata": {"managedFields": [{"manager": "good", "operation": "Update"}]}},  # noqa: E501
        {"metadata": {"managedFields": [{"manager": "good", "operation": "Apply"}]}},  # noqa: E501
        {"metadata": {"managedFields": [{"manager": "bad", "operation": "Apply"}]}},  # noqa: E501
    )
    async def test__clean_managed_fields_does_nothing_if_no_offending_fm(
            self, current):
        await self.ri._clean_managed_fields(current, "mynamespace", "bad")
        self.patchfn.assert_not_called()

    @ddt.data(
        ({"metadata": {"name": "myname", "managedFields": [
            {"manager": "bad", "operation": "Update"}]}}, 0),
        ({"metadata": {"name": "myname", "managedFields": [
            {"manager": "bad", "operation": "Update"},
            {"manager": "bad", "operation": "Apply"}]}}, 0),
        ({"metadata": {"name": "myname", "managedFields": [
            {"manager": "bad", "operation": "Apply"},
            {"manager": "bad", "operation": "Update"}]}}, 1),
        ({"metadata": {"name": "myname", "managedFields": [
            {"manager": "good", "operation": "Update"},
            {"manager": "bad", "operation": "Update"}]}}, 1),
        ({"metadata": {"name": "myname", "managedFields": [
            {"manager": "good", "operation": "Update"},
            {"manager": "bad", "operation": "Apply"},
            {"manager": "bad", "operation": "Update"}]}}, 2),
    )
    async def test__clean_managed_fields_deletes_offending_fm(
            self, data):
        current, index = data
        await self.ri._clean_managed_fields(current, "mynamespace", "bad")
        self.patchfn.assert_called_once_with(
            "myname",
            "mynamespace",
            [{"op": "remove",
                "path": f"/metadata/managedFields/{index}",
              }],
        )


class TestResourceInterfaceWithStatus(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.createfn = unittest.mock.AsyncMock()
        self.readfn = unittest.mock.AsyncMock()
        self.listfn = unittest.mock.AsyncMock()
        self.patchfn = unittest.mock.AsyncMock()
        self.deletefn = unittest.mock.AsyncMock()
        self.replacefn = unittest.mock.AsyncMock()
        self.deletecollfn = unittest.mock.AsyncMock()
        self.readstatusfn = unittest.mock.AsyncMock()
        self.patchstatusfn = unittest.mock.AsyncMock()
        self.replacestatusfn = unittest.mock.AsyncMock()
        self.ri = interfaces.ResourceInterfaceWithStatus(
            "test.yaook.cloud/v1", "testobjects",
            createfn=self.createfn,
            readfn=self.readfn,
            listfn=self.listfn,
            patchfn=self.patchfn,
            replacefn=self.replacefn,
            deletefn=self.deletefn,
            deletecollfn=self.deletecollfn,
            readstatusfn=self.readstatusfn,
            patchstatusfn=self.patchstatusfn,
            replacestatusfn=self.replacestatusfn,
        )

    def test_is_ResourceInterface(self):
        self.assertIsInstance(self.ri, interfaces.ResourceInterface)

    async def test_read_status(self):
        self.readstatusfn.return_value = unittest.mock.sentinel.read_result

        result = await self.ri.read_status(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
        )

        self.readstatusfn.assert_called_once_with(
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.namespace,
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.read_result,
        )

    async def test_patch_status(self):
        self.patchstatusfn.return_value = unittest.mock.sentinel.patch_result

        result = await self.ri.patch_status(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.patch,
            somekey=unittest.mock.sentinel.somevalue,
        )

        self.patchstatusfn.assert_called_once_with(
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.patch,
            somekey=unittest.mock.sentinel.somevalue,
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.patch_result,
        )

    async def test_replace_status(self):
        self.replacestatusfn.return_value = \
            unittest.mock.sentinel.replace_result

        result = await self.ri.replace_status(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.body,
        )

        self.replacestatusfn.assert_called_once_with(
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.body,
        )
        self.assertEqual(
            result,
            unittest.mock.sentinel.replace_result,
        )


@ddt.ddt()
class Testcustom_resource_interface(unittest.IsolatedAsyncioTestCase):
    @unittest.mock.patch("kubernetes_asyncio.client.CustomObjectsApi")
    def setUp(self, CustomObjectsApi):
        names = [
            "create_namespaced_custom_object",
            "get_namespaced_custom_object",
            "list_namespaced_custom_object",
            "patch_namespaced_custom_object",
            "replace_namespaced_custom_object",
            "delete_namespaced_custom_object",
            "get_namespaced_custom_object_status",
            "patch_namespaced_custom_object_status",
            "replace_namespaced_custom_object_status"
        ]
        self.api_mock = unittest.mock.Mock(names)
        for name in names:
            setattr(self.api_mock, name, unittest.mock.AsyncMock())

        CustomObjectsApi.return_value = self.api_mock

        self.coi = interfaces.custom_resource_interface(
            unittest.mock.sentinel.api_group,
            unittest.mock.sentinel.api_version,
            unittest.mock.sentinel.plural,
            unittest.mock.sentinel.api_client,
        )

    def test_is_ResourceInterfaceWithStatus(self):
        self.assertIsInstance(self.coi, interfaces.ResourceInterfaceWithStatus)

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.ResourceInterface.create")
    async def test_create(self, createfn):
        createfn.return_value = \
            unittest.mock.sentinel.created_object

        result = await self.coi.create(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.body,
            unittest.mock.sentinel.fieldmanager,
        )

        createfn.assert_called_once_with(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.body,
            unittest.mock.sentinel.fieldmanager,
        )

        self.assertEqual(
            result,
            unittest.mock.sentinel.created_object,
        )

    async def test_read(self):
        self.api_mock.get_namespaced_custom_object.return_value = \
            unittest.mock.sentinel.read_object

        result = await self.coi.read(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
        )

        self.api_mock.get_namespaced_custom_object.assert_called_once_with(
            unittest.mock.sentinel.api_group,
            unittest.mock.sentinel.api_version,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.plural,
            unittest.mock.sentinel.name,
        )

        self.assertEqual(
            result,
            unittest.mock.sentinel.read_object,
        )

    async def test_list_(self):
        items = [
            unittest.mock.sentinel.o1,
            unittest.mock.sentinel.o2,
            unittest.mock.sentinel.o3,
        ]

        self.api_mock.list_namespaced_custom_object.return_value = {
            "items": list(items),
            "metadata": {
                "resourceVersion": unittest.mock.sentinel.resource_version,
            },
        }

        result = await self.coi.list_(
            unittest.mock.sentinel.namespace,
        )

        self.api_mock.list_namespaced_custom_object.assert_called_once_with(
            unittest.mock.sentinel.api_group,
            unittest.mock.sentinel.api_version,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.plural,
        )

        self.assertEqual(result, items)

    async def test_list_forwards_resource_version(self):
        items = [
            unittest.mock.sentinel.o1,
            unittest.mock.sentinel.o2,
            unittest.mock.sentinel.o3,
        ]

        self.api_mock.list_namespaced_custom_object.return_value = {
            "items": list(items),
            "metadata": {
                "resourceVersion": unittest.mock.sentinel.resource_version,
            },
        }

        result = await self.coi.list_(
            unittest.mock.sentinel.namespace,
        )

        self.api_mock.list_namespaced_custom_object.assert_called_once_with(
            unittest.mock.sentinel.api_group,
            unittest.mock.sentinel.api_version,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.plural,
        )

        self.assertEqual(
            result.resource_version,
            unittest.mock.sentinel.resource_version,
        )

        self.assertIsInstance(result, interfaces.ListWrapper)

    async def test_listfn_passes_kwargs(self):
        items = [
            unittest.mock.sentinel.o1,
            unittest.mock.sentinel.o2,
            unittest.mock.sentinel.o3,
        ]

        self.api_mock.list_namespaced_custom_object.return_value = {
            "items": list(items),
            "metadata": {"resourceVersion": "foo"},
        }

        result = await self.coi._list(
            unittest.mock.sentinel.namespace,
            foo=unittest.mock.sentinel.foo,
            bar=unittest.mock.sentinel.bar,
        )

        self.api_mock.list_namespaced_custom_object.assert_called_once_with(
            unittest.mock.sentinel.api_group,
            unittest.mock.sentinel.api_version,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.plural,
            foo=unittest.mock.sentinel.foo,
            bar=unittest.mock.sentinel.bar,
        )

        self.assertEqual(result, items)

    async def test_patch(self):
        await self.coi.patch(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.patch,
        )

        self.api_mock.patch_namespaced_custom_object.assert_called_once_with(
            unittest.mock.sentinel.api_group,
            unittest.mock.sentinel.api_version,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.plural,
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.patch,
        )

    async def test_replace(self):
        await self.coi.replace(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.body,
        )

        self.api_mock.replace_namespaced_custom_object.assert_called_once_with(
            unittest.mock.sentinel.api_group,
            unittest.mock.sentinel.api_version,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.plural,
            unittest.mock.sentinel.name,
            unittest.mock.sentinel.body,
        )

    async def test_delete(self):
        await self.coi.delete(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
        )

        self.api_mock.delete_namespaced_custom_object.assert_called_once_with(
            unittest.mock.sentinel.api_group,
            unittest.mock.sentinel.api_version,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.plural,
            unittest.mock.sentinel.name,
            propagation_policy="Background"
        )

    @ddt.data(
        (interfaces.DeletionPropagationPolicy.BACKGROUND, "Background"),
        (interfaces.DeletionPropagationPolicy.FOREGROUND, "Foreground"))
    async def test_delete_propagation_policy(self, data):
        await self.coi.delete(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
            propagation_policy=data[0]
        )
        self.api_mock.delete_namespaced_custom_object.assert_called_once_with(
            unittest.mock.sentinel.api_group,
            unittest.mock.sentinel.api_version,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.plural,
            unittest.mock.sentinel.name,
            propagation_policy=data[1]
        )

    async def test_delete_collection_uses_wrapper(self):
        items = [
            unittest.mock.sentinel.item1,
            unittest.mock.sentinel.item2,
            unittest.mock.sentinel.item3,
        ]
        wrapped_items = {
            "items": list(items),
            "metadata": {"resourceVersion": "foo"},
        }

        self.api_mock.list_namespaced_custom_object.return_value = \
            wrapped_items

        def generate_metadata():
            for i in itertools.count():
                yield {
                    "namespace": getattr(unittest.mock.sentinel,
                                         f"namespace{i}"),
                    "name": getattr(unittest.mock.sentinel, f"name{i}"),
                }

        with contextlib.ExitStack() as stack:
            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = generate_metadata()

            await self.coi.delete_collection(
                unittest.mock.sentinel.namespace,
                label_selector=unittest.mock.sentinel.label_selector,
            )

        self.api_mock.list_namespaced_custom_object.assert_awaited_once_with(
            unittest.mock.sentinel.api_group,
            unittest.mock.sentinel.api_version,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.plural,
            label_selector=unittest.mock.sentinel.label_selector,
        )

        self.assertCountEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call(item)
                for item in items
            ],
        )

        self.assertCountEqual(
            self.api_mock.delete_namespaced_custom_object.await_args_list,
            [
                unittest.mock.call(
                    unittest.mock.sentinel.api_group,
                    unittest.mock.sentinel.api_version,
                    unittest.mock.sentinel.namespace0,
                    unittest.mock.sentinel.plural,
                    unittest.mock.sentinel.name0,
                ),
                unittest.mock.call(
                    unittest.mock.sentinel.api_group,
                    unittest.mock.sentinel.api_version,
                    unittest.mock.sentinel.namespace1,
                    unittest.mock.sentinel.plural,
                    unittest.mock.sentinel.name1,
                ),
                unittest.mock.call(
                    unittest.mock.sentinel.api_group,
                    unittest.mock.sentinel.api_version,
                    unittest.mock.sentinel.namespace2,
                    unittest.mock.sentinel.plural,
                    unittest.mock.sentinel.name2,
                ),
            ],
        )


class Testget_selected_nodes_union(unittest.IsolatedAsyncioTestCase):
    def _make_selector(self, api_str):
        selector = unittest.mock.Mock(["as_api_selector"])
        selector.as_api_selector.return_value = api_str
        return selector

    def _node_mock(self, name):
        node = unittest.mock.Mock([])
        node.metadata = unittest.mock.Mock([])
        node.metadata.name = name
        return node

    def _wrap_node_list(self, nodes):
        mock = kubernetes_asyncio.client.V1NodeList(
            items=list(nodes),
            metadata=kubernetes_asyncio.client.V1ListMeta(),
        )
        return mock

    async def test_fetches_all_nodes_by_label_selectors(self):
        def node_list_generator():
            for i in itertools.count():
                yield self._wrap_node_list([
                    self._node_mock("node{}_{}".format(i, j))
                    for j in range(2)
                ])

        selectors = [
            self._make_selector(unittest.mock.sentinel.selector1),
            self._make_selector(unittest.mock.sentinel.selector2),
            self._make_selector(unittest.mock.sentinel.selector3),
        ]

        v1 = unittest.mock.Mock(["list_node"])
        v1.list_node = unittest.mock.AsyncMock()
        v1.list_node.side_effect = node_list_generator()

        with contextlib.ExitStack() as stack:
            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value = v1

            result = await interfaces.get_selected_nodes_union(
                unittest.mock.sentinel.api_client,
                selectors,
            )

        CoreV1Api.assert_called_once_with(unittest.mock.sentinel.api_client)

        self.assertCountEqual(
            v1.list_node.mock_calls,
            [
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector1),
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector2),
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector3),
            ],
        )

        self.assertCountEqual(
            [node.metadata.name for node in result],
            [
                "node0_0",
                "node0_1",
                "node1_0",
                "node1_1",
                "node2_0",
                "node2_1",
            ]
        )

    async def test__get_target_nodes_deduplicates_nodes_by_name(self):
        node_lists = [
            [
                self._node_mock("node0"),
                self._node_mock("node1"),
            ],
            [
                self._node_mock("node1"),
                self._node_mock("node2"),
            ],
            [
                self._node_mock("node2"),
                self._node_mock("node3"),
            ],
        ]

        def node_list_generator():
            for node_list in node_lists:
                yield self._wrap_node_list(node_list)

        selectors = [
            self._make_selector(unittest.mock.sentinel.selector1),
            self._make_selector(unittest.mock.sentinel.selector2),
            self._make_selector(unittest.mock.sentinel.selector3),
        ]

        v1 = unittest.mock.Mock(["list_node"])
        v1.list_node = unittest.mock.AsyncMock()
        v1.list_node.side_effect = node_list_generator()

        with contextlib.ExitStack() as stack:
            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value = v1

            result = await interfaces.get_selected_nodes_union(
                unittest.mock.sentinel.api_client,
                selectors,
            )

        CoreV1Api.assert_called_once_with(unittest.mock.sentinel.api_client)

        self.assertCountEqual(
            v1.list_node.mock_calls,
            [
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector1),
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector2),
                unittest.mock.call(
                    label_selector=unittest.mock.sentinel.selector3),
            ],
        )

        self.assertCountEqual(
            [node.metadata.name for node in result],
            [
                "node0",
                "node1",
                "node2",
                "node3",
            ]
        )


class TestServiceInterfaceCreation(unittest.IsolatedAsyncioTestCase):
    # We need to test services here specifically because they behave
    # differently to all other resources pre 1.23
    def setUp(self):
        self.createfn = unittest.mock.AsyncMock()
        self.readfn = unittest.mock.AsyncMock()
        self.patchfn = unittest.mock.AsyncMock()
        self._clean_managed_fields = unittest.mock.AsyncMock()
        self.api_client = unittest.mock.Mock(
            spec=kubernetes_asyncio.client.ApiClient)
        self.ri = interfaces.service_interface(self.api_client)
        self.ri._create = self.createfn
        self.ri._read = self.readfn
        self.ri._patch = self.patchfn
        self.ri._clean_managed_fields = self._clean_managed_fields

        self.spec = str(uuid.uuid4())
        self.rv1 = f"resource-version-{uuid.uuid4()}-1"
        self.rv2 = f"resource-version-{uuid.uuid4()}-2"

        self.k8s122 = kubernetes_asyncio.client.VersionInfo(
                build_date="whocares",
                compiler="whocares",
                git_commit="whocares",
                git_tree_state="whocares",
                git_version="whocares",
                go_version="whocares",
                major=1,
                minor=22,
                platform="whocares")
        self.k8s123 = kubernetes_asyncio.client.VersionInfo(
                build_date="whocares",
                compiler="whocares",
                git_commit="whocares",
                git_tree_state="whocares",
                git_version="whocares",
                go_version="whocares",
                major=1,
                minor=23,
                platform="whocares")

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.ResourceInterface.create")
    async def test_create_uses_super_if_new(self, super_create):
        body = {
            "metadata": {
                "generateName": "foo-",
            },
            "spec": self.spec,
        }

        with contextlib.ExitStack() as stack:
            get_code = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.VersionApi.get_code",
                new=unittest.mock.AsyncMock()
            ))
            get_code.return_value = self.k8s123

            super_create.return_value = unittest.mock.sentinel.super_create

            result = await self.ri.create(
                unittest.mock.sentinel.namespace,
                body,
                unittest.mock.sentinel.field_manager,
            )

        super_create.assert_called_once_with(
            unittest.mock.sentinel.namespace,
            body,
            unittest.mock.sentinel.field_manager,
        )

        self.createfn.assert_not_called()
        self.patchfn.assert_not_called()
        self._clean_managed_fields.assert_not_called()

        self.assertEqual(result, unittest.mock.sentinel.super_create)

    async def test_create_patches_and_cleans(self):
        body = {
            "metadata": {
                "generateName": "foo-",
            },
            "spec": self.spec,
        }

        with contextlib.ExitStack() as stack:
            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "name": "real-name",
                "resourceVersion": self.rv1,
                "uid": unittest.mock.sentinel.uid1,
            }
            get_code = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.VersionApi.get_code",
                new=unittest.mock.AsyncMock()
            ))
            get_code.return_value = self.k8s122

            self.createfn.return_value = unittest.mock.sentinel.created
            self.patchfn.return_value = unittest.mock.sentinel.patched
            self.readfn.return_value = unittest.mock.sentinel.read

            result = await self.ri.create(
                unittest.mock.sentinel.namespace,
                body,
                unittest.mock.sentinel.field_manager,
            )

        extract_metadata.assert_called_once_with(
            unittest.mock.sentinel.created,
        )

        self.createfn.assert_awaited_once_with(
            unittest.mock.sentinel.namespace,
            {
                "metadata": {
                    "generateName": "foo-",
                },
                "spec": self.spec,
            },
            field_manager=unittest.mock.sentinel.field_manager,
        )

        self.patchfn.assert_called_once_with(
            "real-name",
            unittest.mock.sentinel.namespace,
            json.dumps({
                "metadata": {
                    "generateName": "foo-",
                    "resourceVersion": self.rv1,
                    "name": "real-name",
                },
                "spec": self.spec,
            }).encode("utf-8"),
            field_manager=unittest.mock.sentinel.field_manager,
            force=True,
        )

        self._clean_managed_fields.assert_called_once_with(
            unittest.mock.sentinel.read,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.field_manager,
        )

        self.assertEqual(result, unittest.mock.sentinel.read)

    async def test_create_propagates_non_conflict_errors(self):
        body = {
            "metadata": {
                "generateName": "foo-",
            },
            "spec": self.spec,
        }

        with contextlib.ExitStack() as stack:
            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "name": "real-name",
                "resourceVersion": self.rv1,
                "uid": unittest.mock.sentinel.uid1,
            }
            get_code = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.VersionApi.get_code",
                new=unittest.mock.AsyncMock()
            ))
            get_code.return_value = self.k8s122

            self.createfn.return_value = unittest.mock.sentinel.created
            self.patchfn.side_effect = kubernetes_asyncio.client.ApiException(
                status=unittest.mock.sentinel.status,
            )

            with self.assertRaises(kubernetes_asyncio.client.ApiException):
                await self.ri.create(
                    unittest.mock.sentinel.namespace,
                    body,
                    unittest.mock.sentinel.field_manager,
                )

    async def test_create_reads_and_retries_patch_on_conflict(self):
        body = {
            "metadata": {
                "generateName": "foo-",
            },
            "spec": self.spec,
        }

        def generate_metadata():
            yield {
                "name": "real-name",
                "resourceVersion": self.rv1,
                "uid": unittest.mock.sentinel.uid1,
            }
            yield {
                "name": "real-name",
                "resourceVersion": self.rv2,
                "uid": unittest.mock.sentinel.uid1,
            }

        ctr = 0

        def patch_effect(*args, **kwargs):
            nonlocal ctr
            ctr += 1
            if ctr == 1:
                raise kubernetes_asyncio.client.ApiException(status=409)
            return unittest.mock.sentinel.patched

        with contextlib.ExitStack() as stack:
            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = generate_metadata()
            get_code = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.VersionApi.get_code",
                new=unittest.mock.AsyncMock()
            ))
            get_code.return_value = self.k8s122

            self.createfn.return_value = unittest.mock.sentinel.created
            self.patchfn.side_effect = patch_effect
            self.readfn.return_value = unittest.mock.sentinel.read

            result = await self.ri.create(
                unittest.mock.sentinel.namespace,
                body,
                unittest.mock.sentinel.field_manager,
            )

        self.assertSequenceEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call(unittest.mock.sentinel.created),
                unittest.mock.call(unittest.mock.sentinel.read),
            ],
        )

        self.createfn.assert_awaited_once_with(
            unittest.mock.sentinel.namespace,
            {
                "metadata": {
                    "generateName": "foo-",
                },
                "spec": self.spec,
            },
            field_manager=unittest.mock.sentinel.field_manager,
        )

        self.assertSequenceEqual(
            self.patchfn.await_args_list,
            [
                unittest.mock.call(
                    "real-name",
                    unittest.mock.sentinel.namespace,
                    json.dumps({
                        "metadata": {
                            "generateName": "foo-",
                            "resourceVersion": self.rv1,
                            "name": "real-name",
                        },
                        "spec": self.spec,
                    }).encode("utf-8"),
                    field_manager=unittest.mock.sentinel.field_manager,
                    force=True,
                ),
                unittest.mock.call(
                    "real-name",
                    unittest.mock.sentinel.namespace,
                    json.dumps({
                        "metadata": {
                            "generateName": "foo-",
                            "resourceVersion": self.rv2,
                            "name": "real-name",
                        },
                        "spec": self.spec,
                    }).encode("utf-8"),
                    field_manager=unittest.mock.sentinel.field_manager,
                    force=True,
                ),
            ],
        )

        self._clean_managed_fields.assert_called_once_with(
            unittest.mock.sentinel.read,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.field_manager,
        )

        self.assertEqual(result, unittest.mock.sentinel.read)

    async def test_raises_if_uid_differs_on_readback(self):
        body = {
            "metadata": {
                "generateName": "foo-",
            },
            "spec": self.spec,
        }

        def generate_metadata():
            yield {
                "name": "real-name",
                "resourceVersion": self.rv1,
                "uid": unittest.mock.sentinel.uid1,
            }
            yield {
                "name": "real-name",
                "resourceVersion": self.rv2,
                "uid": unittest.mock.sentinel.uid2,
            }

        ctr = 0

        def patch_effect(*args, **kwargs):
            nonlocal ctr
            ctr += 1
            if ctr == 1:
                raise kubernetes_asyncio.client.ApiException(status=409)
            return unittest.mock.sentinel.patched

        with contextlib.ExitStack() as stack:
            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = generate_metadata()
            get_code = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.VersionApi.get_code",
                new=unittest.mock.AsyncMock()
            ))
            get_code.return_value = self.k8s122

            self.createfn.return_value = unittest.mock.sentinel.created
            self.patchfn.side_effect = patch_effect
            self.readfn.return_value = unittest.mock.sentinel.readback1

            with self.assertRaisesRegex(
                    kubernetes_asyncio.client.ApiException,
                    "create/delete conflict: got different uid during "
                    "readback",
                    ) as info:
                await self.ri.create(
                    unittest.mock.sentinel.namespace,
                    body,
                    unittest.mock.sentinel.field_manager,
                )

        self.assertSequenceEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call(unittest.mock.sentinel.created),
                unittest.mock.call(unittest.mock.sentinel.readback1),
            ],
        )

        self.createfn.assert_awaited_once_with(
            unittest.mock.sentinel.namespace,
            {
                "metadata": {
                    "generateName": "foo-",
                },
                "spec": self.spec,
            },
            field_manager=unittest.mock.sentinel.field_manager,
        )

        self.patchfn.assert_awaited_once_with(
            "real-name",
            unittest.mock.sentinel.namespace,
            json.dumps({
                "metadata": {
                    "generateName": "foo-",
                    "resourceVersion": self.rv1,
                    "name": "real-name",
                },
                "spec": self.spec,
            }).encode("utf-8"),
            field_manager=unittest.mock.sentinel.field_manager,
            force=True,
        )

        self._clean_managed_fields.assert_not_called()

        self.assertEqual(info.exception.status, 409)
