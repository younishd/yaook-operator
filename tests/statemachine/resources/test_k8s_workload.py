import contextlib
import copy
import unittest
import unittest.mock
import uuid
from unittest.mock import sentinel

import ddt

import kubernetes_asyncio.client as kclient

import yaook.statemachine.context as context
import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.interfaces as interfaces
import yaook.statemachine.watcher as watcher

import yaook.statemachine.resources.k8s_workload as k8s_workload

from .utils import ResourceTestMixin, ResourceInterfaceMock


@ddt.ddt()
class TestJob(unittest.IsolatedAsyncioTestCase):
    class JobTest(ResourceTestMixin, k8s_workload.Job):
        pass

    def setUp(self):
        self.component = str(uuid.uuid4())
        self.ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.j = self.JobTest(component=self.component,
                              scheduling_keys=[])

    def test_raises_when_copy_on_write_set(self):
        self.assertRaises(ValueError, self.JobTest, copy_on_write=sentinel.cow,
                          scheduling_keys=[])

    def test___init___sets__ignore_deleted_resources(self):
        self.assertFalse(self.j._ignore_deleted_resources)

    @unittest.mock.patch(
        "yaook.statemachine.api_utils.k8s_obj_to_yaml_data")
    @unittest.mock.patch(
        "yaook.statemachine.api_utils.deep_has_changes")
    @ddt.data(True, False)
    def test_needs_update_compares_spec(self, data, deep_has_changes,
                                        k8s_obj_to_yaml_data):
        lhs = unittest.mock.Mock(["spec"])
        k8s_obj_to_yaml_data.return_value = {"spec": {"abc": 123}}
        deep_has_changes.return_value = data

        result = self.j._needs_update(
            lhs,
            {"spec": unittest.mock.sentinel.rhs}
        )

        k8s_obj_to_yaml_data.assert_called_with(
            lhs
        )

        deep_has_changes.assert_called_once_with(
            {"abc": 123},
            unittest.mock.sentinel.rhs
        )

        self.assertEqual(result, data)

    def test__needs_update_returns_false_on_the_same_labels(self):
        lhs = kclient.V1Job(
            spec=kclient.V1JobSpec(
                template=kclient.V1Pod(),
            ),
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )
        rhs = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                },
            },
        }

        self.assertFalse(self.j._needs_update(lhs, rhs))

    def test__needs_update_returns_true_on_label_change(self):
        lhs = kclient.V1Job(
            spec=kclient.V1JobSpec(
                template=kclient.V1Pod(),
            ),
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )
        rhs = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                    "bar": "bar",
                },
            },
        }

        self.assertTrue(self.j._needs_update(lhs, rhs))

    def test_registers_listener_for_jobs(self):
        self.assertCountEqual(
            self.j.get_listeners(),
            [
                context.KubernetesListener(
                    'batch', 'v1', 'jobs', self.j._handle_job_event,
                    component=self.component,
                )
            ]
        )

    async def test_handle_job_event_triggers_reconcile_if_job_is_deleted(
            self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.DELETED

        result = self.j._handle_job_event(
            self.ctx,
            ev,
        )

        self.assertTrue(result)

    async def test_handle_job_event_does_nothing_if_no_status(self):
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = None

        result = self.j._handle_job_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)

    async def test_handle_job_event_does_nothing_if_no_succeeded(self):
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.succeeded = None

        result = self.j._handle_job_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)

    async def test_handle_job_event_does_nothing_if_succeeded_and_annotated(
            self):
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.succeeded = 1
        ev.object_.metadata = unittest.mock.Mock([])
        ev.object_.metadata.annotations = {
            k8s_workload.Job._OBSERVATION_STATE_ANNOTATION:
                str(uuid.uuid4()),
        }

        result = self.j._handle_job_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)

    async def test_handle_job_event_triggers_reconcile_if_annotation_is_missing(self):  # NOQA
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.succeeded = 1
        ev.object_.metadata = unittest.mock.Mock([])
        ev.object_.metadata.annotations = None

        result = self.j._handle_job_event(
            self.ctx,
            ev,
        )

        self.assertTrue(result)

    async def test__update_resource_deletes_job(self):
        with contextlib.ExitStack() as stack:
            delete = unittest.mock.AsyncMock()
            read = unittest.mock.AsyncMock()
            ri = unittest.mock.Mock(["delete", "read"])
            ri.delete = delete
            ri.read = read
            get_resource_interface = stack.enter_context(
                unittest.mock.patch.object(
                    self.j, "get_resource_interface"
                ))
            get_resource_interface.return_value = ri

            _orphan = stack.enter_context(unittest.mock.patch.object(
                self.j, "_orphan",
                new=unittest.mock.AsyncMock()
            ))

            ret = await self.j._update_resource(
                self.ctx,
                {"metadata": {"name": unittest.mock.sentinel.name}},
                unittest.mock.sentinel.new,
            )

        _orphan.assert_not_called()
        delete.assert_called_once_with(
            self.ctx.namespace,
            unittest.mock.sentinel.name,
            interfaces.DeletionPropagationPolicy.FOREGROUND)
        read.assert_called_once_with(self.ctx.namespace,
                                     unittest.mock.sentinel.name)
        self.assertTrue(ret)

    async def test__update_resource_deletes_job_continues_if_gone_fast(self):
        with contextlib.ExitStack() as stack:
            delete = unittest.mock.AsyncMock()
            read = unittest.mock.AsyncMock()
            read.side_effect = kclient.ApiException(404)
            ri = unittest.mock.Mock(["delete", "read"])
            ri.delete = delete
            ri.read = read
            get_resource_interface = stack.enter_context(
                unittest.mock.patch.object(
                    self.j, "get_resource_interface"
                ))
            get_resource_interface.return_value = ri

            _orphan = stack.enter_context(unittest.mock.patch.object(
                self.j, "_orphan",
                new=unittest.mock.AsyncMock()
            ))

            ret = await self.j._update_resource(
                self.ctx,
                {"metadata": {"name": unittest.mock.sentinel.name}},
                unittest.mock.sentinel.new,
            )

        _orphan.assert_not_called()
        delete.assert_called_once_with(
            self.ctx.namespace,
            unittest.mock.sentinel.name,
            interfaces.DeletionPropagationPolicy.FOREGROUND)
        read.assert_called_once_with(self.ctx.namespace,
                                     unittest.mock.sentinel.name)
        self.assertFalse(ret)

    async def test__update_resource_deletes_job_raises_on_read_fail(self):
        with contextlib.ExitStack() as stack:
            delete = unittest.mock.AsyncMock()
            read = unittest.mock.AsyncMock()
            read.side_effect = kclient.ApiException(543)
            ri = unittest.mock.Mock(["delete", "read"])
            ri.delete = delete
            ri.read = read
            get_resource_interface = stack.enter_context(
                unittest.mock.patch.object(
                    self.j, "get_resource_interface"
                ))
            get_resource_interface.return_value = ri

            _orphan = stack.enter_context(unittest.mock.patch.object(
                self.j, "_orphan",
                new=unittest.mock.AsyncMock()
            ))

            with self.assertRaises(kclient.ApiException):
                await self.j._update_resource(
                    self.ctx,
                    {"metadata": {"name": unittest.mock.sentinel.name}},
                    unittest.mock.sentinel.new,
                )

        _orphan.assert_not_called()
        delete.assert_called_once_with(
            self.ctx.namespace,
            unittest.mock.sentinel.name,
            interfaces.DeletionPropagationPolicy.FOREGROUND)
        read.assert_called_once_with(self.ctx.namespace,
                                     unittest.mock.sentinel.name)

    async def test_is_ready_returns_false_if_nonexistent(self):
        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.side_effect = exceptions.ResourceNotPresent(
                unittest.mock.sentinel.component,
                unittest.mock.Mock(),
            )

            result = await self.j.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    async def test_is_ready_returns_false_if_being_deleted(self):
        job = unittest.mock.Mock(["metadata"])
        job.metadata = unittest.mock.Mock(["to_dict"])
        job.metadata.to_dict.return_value = {
            "deletionTimestamp": sentinel.deleted}

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = job

            result = await self.j.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    async def test_is_ready_returns_false_if_no_status(self):
        job = unittest.mock.Mock(["status"])
        job.metadata = unittest.mock.Mock(["to_dict"])
        job.metadata.to_dict.return_value = {}
        job.status = None

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = job

            result = await self.j.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    async def test_is_ready_returns_false_if_no_succeeded(self):
        job = unittest.mock.Mock(["status"])
        job.metadata = unittest.mock.Mock(["to_dict"])
        job.metadata.to_dict.return_value = {}
        job.status.succeeded = 0

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = job

            result = await self.j.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    async def test_is_ready_returns_true_if_succeeded_and_annotation_is_present(self):  # NOQA
        job = unittest.mock.Mock(["status"])
        job.metadata = unittest.mock.Mock(["annotations", "to_dict"])
        job.metadata.to_dict.return_value = {}
        job.metadata.annotations = {
            k8s_workload.Job._OBSERVATION_STATE_ANNOTATION:
                str(uuid.uuid4())
        }
        job.status.succeeded = 1

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = job

            result = await self.j.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertTrue(result)

    async def test_is_ready_returns_true_and_patches_annotation_if_missing(
            self):
        job = unittest.mock.Mock(["status"])
        job.metadata = unittest.mock.Mock(["namespace", "name", "to_dict"])
        job.metadata.to_dict.return_value = {}
        job.metadata.annotations = {}
        job.status.succeeded = 1

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = job

            result = await self.j.is_ready(self.ctx)

        get_current.assert_called_once_with(self.ctx)
        self.assertTrue(result)

        self.j.resource_interface_mock.patch.assert_called_once_with(
            job.metadata.namespace,
            job.metadata.name,
            [
                {"op": "add",
                 "path": "/metadata/annotations/{}".format(
                     "statemachine.yaook.cloud~1completion-observed",
                 ),
                 "value": "true"},
            ],
        )

    async def test_adopt_does_nothing_new_if_no_scheduling_keys_are_set(self):
        object_ = {
            "metadata": {},
            "spec": {},
        }
        object_bak = copy.deepcopy(object_)

        with unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object",
                ) as adopt_object:
            await self.j.adopt_object(
                unittest.mock.sentinel.ctx,
                object_,
            )

        self.assertEqual(object_, object_bak)
        adopt_object.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            object_,
        )

    async def test_adopt_uses_inject_scheduling_keys(self):
        object_ = {
            "spec": {
                "template": {
                    "spec": unittest.mock.sentinel.pod_spec,
                }
            }
        }

        j = self.JobTest(
            component=self.component,
            scheduling_keys=["foo", "bar"],
        )

        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object",
            ))

            inject_scheduling_keys = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.inject_scheduling_keys",
            ))

            await j.adopt_object(
                unittest.mock.sentinel.ctx,
                object_,
            )

        adopt_object.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            object_,
        )
        inject_scheduling_keys.assert_called_once_with(
            unittest.mock.sentinel.pod_spec,
            ["foo", "bar"],
        )

    async def test_adopt_autocreates_pod_spec_if_necessary(self):
        object_ = {}

        j = self.JobTest(
            component=self.component,
            scheduling_keys=["foo", "bar"],
        )

        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object",
            ))

            inject_scheduling_keys = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.inject_scheduling_keys",
            ))

            await j.adopt_object(
                unittest.mock.sentinel.ctx,
                object_,
            )

        adopt_object.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            object_,
        )
        inject_scheduling_keys.assert_called_once_with(
            object_["spec"]["template"]["spec"],
            ["foo", "bar"],
        )

    async def test_get_used_resources_returns_empty_for_absent_job(self):
        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
            ))
            _get_current.side_effect = exceptions.ResourceNotPresent(
                unittest.mock.sentinel.foo,
                unittest.mock.Mock(),
            )

            result = await self.j.get_used_resources(self.ctx)

        self.assertCountEqual(
            result,
            [],
        )

        _get_current.assert_called_once_with(self.ctx)

    async def test_get_used_resources_returns_empty_for_successful_job(self):
        j = kclient.V1Job(status=kclient.V1JobStatus(
            succeeded=1,
        ))

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
            ))
            _get_current.return_value = j

            result = await self.j.get_used_resources(self.ctx)

        self.assertCountEqual(
            result,
            [],
        )

    async def test_get_used_resources_extracts_resources_from_pod_spec_for_job_without_succeeded(self):  # NOQA
        j = kclient.V1Job(
            spec=kclient.V1JobSpec(
                selector=unittest.mock.sentinel.selector,
                template=kclient.V1Pod(
                    spec=unittest.mock.sentinel.pod_spec,
                ),
            ),
        )

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
            ))
            _get_current.return_value = j

            extract_pod_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_pod_references",
            ))
            extract_pod_references.return_value = [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ]

            pod_interface_mock = ResourceInterfaceMock()
            pod_interface = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.pod_interface",
            ))
            pod_interface.return_value = pod_interface_mock
            pod_interface_mock.list_.return_value = []

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.LabelSelector.from_api_object",
            ))

            result = await self.j.get_used_resources(self.ctx)

        extract_pod_references.assert_called_once_with(
            unittest.mock.sentinel.pod_spec,
            self.ctx.namespace,
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ],
        )

    async def test_get_used_resources_extracts_resources_from_existing_pods_for_job_without_succeeded(self):  # NOQA
        j = kclient.V1Job(
            spec=kclient.V1JobSpec(
                selector=unittest.mock.sentinel.selector,
                template=kclient.V1Pod(
                    spec=unittest.mock.sentinel.pod_spec,
                ),
            ),
            status=kclient.V1JobStatus(succeeded=0)
        )

        def generate_resources():
            yield [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ]
            yield [
                unittest.mock.sentinel.r1p1,
                unittest.mock.sentinel.r2p1,
            ]
            yield [
                unittest.mock.sentinel.r1p2,
                unittest.mock.sentinel.r2p2,
            ]
            yield [
                unittest.mock.sentinel.r1p3,
                unittest.mock.sentinel.r2p3,
            ]

        def mock_pod(spec):
            return kclient.V1Pod(spec=spec)

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
            ))
            _get_current.return_value = j

            extract_pod_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_pod_references",
            ))
            extract_pod_references.side_effect = generate_resources()

            pod_interface_mock = ResourceInterfaceMock()
            pod_interface = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.pod_interface",
            ))
            pod_interface.return_value = pod_interface_mock
            pod_interface_mock.list_.return_value = [
                mock_pod(unittest.mock.sentinel.pod1),
                mock_pod(unittest.mock.sentinel.pod2),
                mock_pod(unittest.mock.sentinel.pod3),
            ]

            from_api_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.LabelSelector.from_api_object",
            ))

            result = await self.j.get_used_resources(self.ctx)

        from_api_object.assert_called_once_with(
            unittest.mock.sentinel.selector,
        )

        pod_interface.assert_called_once_with(
            self.ctx.api_client,
        )

        pod_interface_mock.list_.assert_awaited_once_with(
            self.ctx.namespace,
            label_selector=from_api_object().as_api_selector(),
        )

        self.assertCountEqual(
            extract_pod_references.mock_calls,
            [
                unittest.mock.call(unittest.mock.sentinel.pod_spec,
                                   self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod1,
                                   self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod2,
                                   self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod3,
                                   self.ctx.namespace),
            ],
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
                unittest.mock.sentinel.r1p1,
                unittest.mock.sentinel.r2p1,
                unittest.mock.sentinel.r1p2,
                unittest.mock.sentinel.r2p2,
                unittest.mock.sentinel.r1p3,
                unittest.mock.sentinel.r2p3,
            ],
        )


@ddt.ddt()
class TestFinalTemplatedJob(unittest.IsolatedAsyncioTestCase):
    class JobTest(ResourceTestMixin, k8s_workload.FinalTemplatedJob):
        pass

    def setUp(self):
        self.component = str(uuid.uuid4())
        self.ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.j = self.JobTest(component=self.component,
                              scheduling_keys=[],
                              template="abc")

    @ddt.data(
        # succeeded, failed, should_trigger
        (0, 0, False),
        (1, 0, True),
        (0, 1, True)
    )
    async def test__handle_job_event_triggers_for_failed_or_success(self,
                                                                    data):
        succeeded, failed, expected_result = data
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.succeeded = succeeded
        ev.object_.status.failed = failed
        ev.object_.metadata = unittest.mock.Mock([])
        ev.object_.metadata.annotations = None

        result = self.j._handle_job_event(
            self.ctx,
            ev,
        )

        self.assertEqual(expected_result, result)

    @ddt.data(
        # succeeded, failed, annotation, is_ready
        (0, 0, False, False),
        (0, 1, False, True),
        (1, 0, False, True),
        (0, 1, True, True),
        (1, 0, True, True),
    )
    async def test_is_ready(self, data):  # NOQA
        succeeded, failed, annotation_exists, expected_result = data
        job = unittest.mock.Mock(["status"])
        job.metadata = unittest.mock.Mock(["namespace", "name"])
        job.metadata.annotations = {}
        if annotation_exists:
            job.metadata.annotations = {
                k8s_workload.Job._OBSERVATION_STATE_ANNOTATION:
                    str(uuid.uuid4())
            }
        job.status.succeeded = succeeded
        job.status.failed = failed

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = job

            result = await self.j.is_ready(self.ctx)

        get_current.assert_called_once_with(self.ctx)
        self.assertEqual(expected_result, result)

        if not annotation_exists and expected_result:
            self.j.resource_interface_mock.patch.assert_called_once_with(
                job.metadata.namespace,
                job.metadata.name,
                [
                    {"op": "add",
                     "path": "/metadata/annotations/{}".format(
                        "statemachine.yaook.cloud~1completion-observed",
                     ),
                     "value": "true"},
                ],
            )
        else:
            self.j.resource_interface_mock.patch.assert_not_called()

    @ddt.data(
        # succeeded, failed, annotation, is_ready
        (0, 0, False),
        (0, 1, False),
        (1, 0, True),
    )
    async def test_is_successful(self, data):  # NOQA
        succeeded, failed,  expected_result = data
        job = unittest.mock.Mock(["status"])
        job.metadata = unittest.mock.Mock()
        job.status.succeeded = succeeded
        job.status.failed = failed

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.j, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = job

            result = await self.j.is_successful(self.ctx)

        get_current.assert_called_once_with(self.ctx)
        self.assertEqual(expected_result, result)


@ddt.ddt()
class TestDeployment(unittest.IsolatedAsyncioTestCase):
    class DeploymentTest(ResourceTestMixin, k8s_workload.Deployment):
        pass

    def setUp(self):
        self.component = str(uuid.uuid4())
        self.ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.d = self.DeploymentTest(component=self.component,
                                     scheduling_keys=[])

    @unittest.mock.patch(
        "yaook.statemachine.api_utils.k8s_obj_to_yaml_data")
    @unittest.mock.patch(
        "yaook.statemachine.api_utils.deep_has_changes")
    @ddt.data(True, False)
    def test_needs_update_compares_spec(self, data, deep_has_changes,
                                        k8s_obj_to_yaml_data):
        lhs = unittest.mock.Mock(["spec"])
        k8s_obj_to_yaml_data.return_value = {"abc": 123}
        deep_has_changes.return_value = data

        result = self.d._needs_update(
            lhs,
            {"spec": {"foo": unittest.mock.sentinel.rhs}},
        )

        k8s_obj_to_yaml_data.assert_called_with(
            lhs
        )

        deep_has_changes.assert_called_once_with(
            {"abc": 123},
            {"foo": unittest.mock.sentinel.rhs},
        )

        self.assertEqual(result, data)

    def test__needs_update_returns_false_on_the_same_labels(self):
        lhs = kclient.V1Deployment(
            spec=kclient.V1DeploymentSpec(
                selector=kclient.V1LabelSelector(),
                template=kclient.V1PodTemplate()
            ),
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )
        rhs = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                },
            },
        }

        self.assertFalse(self.d._needs_update(lhs, rhs))

    def test__needs_update_returns_true_on_label_change(self):
        lhs = kclient.V1Deployment(
            spec=kclient.V1DeploymentSpec(
                selector=kclient.V1LabelSelector(),
                template=kclient.V1PodTemplate()
            ),
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )
        rhs = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                    "bar": "bar",
                },
            },
        }

        self.assertTrue(self.d._needs_update(lhs, rhs))

    def test_needs_update_ignores_restart_id_annotation_on_pod_template(self):
        lhs = kclient.V1Deployment(
            spec=kclient.V1DeploymentSpec(
                selector=kclient.V1LabelSelector(),
                template=kclient.V1PodTemplate(
                    metadata=kclient.V1ObjectMeta(
                        annotations={
                            context.ANNOTATION_RESTART_ID: "foo",
                        }
                    )
                )
            )
        )
        rhs = {
            "spec": {
                "selector": {},
                "template": {"metadata": {
                    "annotations": {
                        context.ANNOTATION_RESTART_ID: "bar",
                    },
                }},
            },
        }

        self.assertFalse(self.d._needs_update(lhs, rhs))

    async def test_rolling_restart_sets_annotation(self):
        ctx = unittest.mock.Mock(["api_client"])
        deployment = unittest.mock.Mock(["metadata", "spec"])
        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = deployment

            await self.d.rolling_restart(ctx, sentinel.annotation)

        get_current.assert_called_once_with(ctx)
        self.d.resource_interface_mock.patch.assert_called_once_with(
            deployment.metadata.namespace,
            deployment.metadata.name,
            [
                {
                    "op": "add",
                    "path": (
                        "/spec/template/metadata/annotations"
                        "/kubectl.kubernetes.io~1restartedAt"
                    ),
                    "value": sentinel.annotation,
                }
            ]
        )

    async def test_scale_sets_replicas(self):
        ctx = unittest.mock.Mock(["api_client"])
        deployment = unittest.mock.Mock(["metadata"])
        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = deployment

            await self.d.scale(ctx, sentinel.replicas)

        get_current.assert_called_once_with(ctx)
        self.d.resource_interface_mock.patch.assert_called_once_with(
            deployment.metadata.namespace,
            deployment.metadata.name,
            [
                {
                    "op": "replace",
                    "path": (
                        "/spec/replicas"
                    ),
                    "value": sentinel.replicas,
                }
            ]
        )

    async def test_deployment_has_zero_replicas_returns_false_if_no_status(self):  # NOQA
        deployment = unittest.mock.Mock(["status"])
        deployment.status = None

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = deployment

            result = await self.d.deployment_has_zero_replicas(
                unittest.mock.sentinel.ctx
            )

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    async def test_deployment_has_zero_replicas_returns_true_if_zero_replicas(self):  # NOQA
        deployment = unittest.mock.Mock(["status", "spec"])
        deployment.spec.replicas = 0
        deployment.status.replicas = 0

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = deployment

            result = await self.d.deployment_has_zero_replicas(
                unittest.mock.sentinel.ctx
            )

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertTrue(result)

    async def test_adopt_does_nothing_new_if_no_scheduling_keys_are_set(self):
        object_ = {
            "metadata": {},
            "spec": {},
        }
        object_bak = copy.deepcopy(object_)

        with unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object"
                ) as adopt_object:
            await self.d.adopt_object(
                unittest.mock.sentinel.ctx,
                object_,
            )

        self.assertEqual(object_, object_bak)
        adopt_object.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            object_,
        )

    async def test_adopt_uses_inject_scheduling_keys(self):
        object_ = {
            "spec": {
                "template": {
                    "spec": unittest.mock.sentinel.pod_spec,
                }
            }
        }

        d = self.DeploymentTest(
            component=self.component,
            scheduling_keys=["foo", "bar"],
        )

        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object",
            ))

            inject_scheduling_keys = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.inject_scheduling_keys",
            ))

            await d.adopt_object(
                unittest.mock.sentinel.ctx,
                object_,
            )

        adopt_object.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            object_,
        )
        inject_scheduling_keys.assert_called_once_with(
            unittest.mock.sentinel.pod_spec,
            ["foo", "bar"],
        )

    async def test_adopt_autocreates_pod_spec_if_necessary(self):
        object_ = {}

        d = self.DeploymentTest(
            component=self.component,
            scheduling_keys=["foo", "bar"],
        )

        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object",
            ))

            inject_scheduling_keys = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.inject_scheduling_keys",
            ))

            await d.adopt_object(
                unittest.mock.sentinel.ctx,
                object_,
            )

        adopt_object.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            object_,
        )
        inject_scheduling_keys.assert_called_once_with(
            object_["spec"]["template"]["spec"],
            ["foo", "bar"],
        )

    async def test_get_used_resources_returns_empty_for_absent_deployment(self):  # NOQA
        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
            ))
            _get_current.side_effect = exceptions.ResourceNotPresent(
                unittest.mock.sentinel.foo,
                unittest.mock.Mock(),
            )

            result = await self.d.get_used_resources(self.ctx)

        _get_current.assert_called_once_with(self.ctx)

        self.assertCountEqual(result, [])

    async def test_get_used_resources_extracts_resources_from_deployment_pod_spec(self):  # NOQA
        pod_spec = unittest.mock.sentinel.pod_spec

        d = kclient.V1Deployment(spec=kclient.V1DeploymentSpec(
            selector={},
            template=kclient.V1Pod(spec=pod_spec),
        ))

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
            ))
            _get_current.return_value = d

            extract_pod_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_pod_references",
            ))
            extract_pod_references.return_value = [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ]

            pod_interface_mock = ResourceInterfaceMock()
            pod_interface = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.pod_interface",
            ))
            pod_interface.return_value = pod_interface_mock
            pod_interface_mock.list_.return_value = []

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.LabelSelector.from_api_object",
            ))

            result = await self.d.get_used_resources(self.ctx)

        extract_pod_references.assert_called_once_with(
            pod_spec,
            self.ctx.namespace,
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ],
        )

    async def test_get_used_resources_extracts_resources_from_existing_pods(self):  # NOQA
        pod_spec = unittest.mock.sentinel.pod_spec

        d = kclient.V1Deployment(spec=kclient.V1DeploymentSpec(
            selector=unittest.mock.sentinel.selector,
            template=kclient.V1Pod(spec=pod_spec),
        ))

        def generate_resources():
            yield [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ]
            yield [
                unittest.mock.sentinel.r1p1,
                unittest.mock.sentinel.r2p1,
            ]
            yield [
                unittest.mock.sentinel.r1p2,
                unittest.mock.sentinel.r2p2,
            ]
            yield [
                unittest.mock.sentinel.r1p3,
                unittest.mock.sentinel.r2p3,
            ]

        def mock_pod(spec):
            return kclient.V1Pod(spec=spec)

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
            ))
            _get_current.return_value = d

            extract_pod_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_pod_references",
            ))
            extract_pod_references.side_effect = generate_resources()

            pod_interface_mock = ResourceInterfaceMock()
            pod_interface = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.pod_interface",
            ))
            pod_interface.return_value = pod_interface_mock
            pod_interface_mock.list_.return_value = [
                mock_pod(unittest.mock.sentinel.pod1),
                mock_pod(unittest.mock.sentinel.pod2),
                mock_pod(unittest.mock.sentinel.pod3),
            ]

            from_api_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.LabelSelector.from_api_object",
            ))

            result = await self.d.get_used_resources(self.ctx)

        from_api_object.assert_called_once_with(
            unittest.mock.sentinel.selector,
        )

        pod_interface.assert_called_once_with(
            self.ctx.api_client,
        )

        pod_interface_mock.list_.assert_awaited_once_with(
            self.ctx.namespace,
            label_selector=from_api_object().as_api_selector(),
        )

        self.assertCountEqual(
            extract_pod_references.mock_calls,
            [
                unittest.mock.call(pod_spec, self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod1,
                                   self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod2,
                                   self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod3,
                                   self.ctx.namespace),
            ],
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
                unittest.mock.sentinel.r1p1,
                unittest.mock.sentinel.r2p1,
                unittest.mock.sentinel.r1p2,
                unittest.mock.sentinel.r2p2,
                unittest.mock.sentinel.r1p3,
                unittest.mock.sentinel.r2p3,
            ],
        )

    async def test_is_ready_returns_false_if_nonexistent(self):
        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.side_effect = exceptions.ResourceNotPresent(
                unittest.mock.sentinel.component,
                unittest.mock.Mock(),
            )

            result = await self.d.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    async def test_is_ready_returns_false_if_no_status(self):
        deployment = unittest.mock.Mock(["status"])
        deployment.status = None

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = deployment

            result = await self.d.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    async def test_is_ready_returns_true_if_up_to_date(self):
        deployment = unittest.mock.Mock(["status"])
        deployment.metadata = unittest.mock.Mock([])
        deployment.metadata.generation = 123
        deployment.status.observed_generation = 123
        deployment.status.replicas = 3
        deployment.status.updated_replicas = 3
        deployment.status.available_replicas = 3

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = deployment

            result = await self.d.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertTrue(result)

    @ddt.data(
        # generation, observed_generation, replicas, updated, available
        (123, 111, 3, 3, 3),
        (123, 123, 3, 2, 2),
        (123, 123, 3, 3, 2),
        (123, 123, 3, 3, 4),
        (123, 123, 3, 2, 3),
    )
    async def test_is_ready_returns_false_if_not_up_to_date(self, data):
        deployment = unittest.mock.Mock(["status"])
        deployment.metadata = unittest.mock.Mock([])
        deployment.metadata.generation = data[0]
        deployment.status.observed_generation = data[1]
        deployment.status.replicas = data[2]
        deployment.status.updated_replicas = data[3]
        deployment.status.available_replicas = data[4]

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.d, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = deployment

            result = await self.d.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    def test_registers_listener_for_deployments(self):
        self.assertCountEqual(
            self.d.get_listeners(),
            [
                context.KubernetesListener(
                    'apps', 'v1', 'deployments',
                    self.d._handle_deployment_event,
                    component=self.component,
                )
            ]
        )

    async def test_handle_deployment_event_triggers_reconcile_if_deleted(
            self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.DELETED

        result = self.d._handle_deployment_event(
            self.ctx,
            ev,
        )

        self.assertTrue(result)

    async def test_handle_deployment_event_does_nothing_if_no_status(self):
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = None

        result = self.d._handle_deployment_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)

    async def test_handle_deployment_event_does_nothing_if_no_old_status(self):
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = {}
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.status = None

        result = self.d._handle_deployment_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)

    async def test_handle_deployment_event_does_nothing_if_outdated_replicas(self):  # noqa: E501
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.replicas = 3
        ev.object_.status.updated_replicas = 2
        ev.object_.status.available_replicas = 3
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.status = {}

        result = self.d._handle_deployment_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)

    async def test_handle_deployment_event_does_nothing_if_down_replicas(self):  # noqa: E501
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.replicas = 3
        ev.object_.status.updated_replicas = 3
        ev.object_.status.available_replicas = 2
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.status = {}

        result = self.d._handle_deployment_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)

    async def test_handle_deployment_event_reconciles_if_replicas_changed(self):  # noqa: E501
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.metadata = unittest.mock.Mock([])
        ev.object_.metadata.generation = 123
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.replicas = 3
        ev.object_.status.updated_replicas = 3
        ev.object_.status.available_replicas = 3
        ev.object_.status.observed_generation = 123
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.status = unittest.mock.Mock([])
        ev.old_object.status.updated_replicas = 3
        ev.old_object.status.available_replicas = 2
        ev.old_object.status.observed_generation = 123

        result = self.d._handle_deployment_event(
            self.ctx,
            ev,
        )

        self.assertTrue(result)

    async def test_handle_deployment_event_reconciles_if_generation_changed(self):  # noqa: E501
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.metadata = unittest.mock.Mock([])
        ev.object_.metadata.generation = 123
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.replicas = 3
        ev.object_.status.updated_replicas = 3
        ev.object_.status.available_replicas = 3
        ev.object_.status.observed_generation = 123
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.status = unittest.mock.Mock([])
        ev.old_object.status.updated_replicas = 3
        ev.old_object.status.available_replicas = 3
        ev.old_object.status.observed_generation = 122

        result = self.d._handle_deployment_event(
            self.ctx,
            ev,
        )

        self.assertTrue(result)

    async def test_handle_deployment_event_does_nothing_if_replicas_and_generation_not_changed(self):  # noqa: E501
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.metadata = unittest.mock.Mock([])
        ev.object_.metadata.generation = 123
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.replicas = 3
        ev.object_.status.updated_replicas = 3
        ev.object_.status.available_replicas = 3
        ev.object_.status.observed_generation = 123
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.status = unittest.mock.Mock([])
        ev.old_object.status.updated_replicas = 3
        ev.old_object.status.available_replicas = 3
        ev.old_object.status.observed_generation = 123

        result = self.d._handle_deployment_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)


@ddt.ddt()
class TestStatefulSet(unittest.IsolatedAsyncioTestCase):
    class StatefulSetTest(ResourceTestMixin, k8s_workload.StatefulSet):
        pass

    def setUp(self):
        self.component = str(uuid.uuid4())
        self.ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.ss = self.StatefulSetTest(
            component=self.component,
        )

    @unittest.mock.patch(
        "yaook.statemachine.api_utils.k8s_obj_to_yaml_data")
    @unittest.mock.patch(
        "yaook.statemachine.api_utils.deep_has_changes")
    @ddt.data(True, False)
    def test_needs_update_compares_spec(self, data, deep_has_changes,
                                        k8s_obj_to_yaml_data):
        lhs = unittest.mock.Mock(["spec"])
        k8s_obj_to_yaml_data.return_value = {"abc": 123}
        deep_has_changes.return_value = data

        result = self.ss._needs_update(
            lhs,
            {"spec": {"somekey": unittest.mock.sentinel.rhs}}
        )

        k8s_obj_to_yaml_data.assert_called_with(
            lhs
        )

        deep_has_changes.assert_called_once_with(
            {"abc": 123},
            {"somekey": unittest.mock.sentinel.rhs},
        )

        self.assertEqual(result, data)

    def test_needs_update_ignores_restart_id_annotation_on_pod_template(self):
        lhs = kclient.V1StatefulSet(
            spec=kclient.V1StatefulSetSpec(
                selector=kclient.V1LabelSelector(),
                service_name="baz",
                template=kclient.V1PodTemplate(
                    metadata=kclient.V1ObjectMeta(
                        annotations={
                            context.ANNOTATION_RESTART_ID: "foo",
                        }
                    )
                )
            )
        )
        rhs = {
            "spec": {
                "serviceName": "baz",
                "selector": {},
                "template": {"metadata": {
                    "annotations": {
                        context.ANNOTATION_RESTART_ID: "bar",
                    },
                }},
            },
        }

        self.assertFalse(self.ss._needs_update(lhs, rhs))

    def test__needs_update_returns_false_on_the_same_labels(self):
        lhs = kclient.V1StatefulSet(
            spec=kclient.V1StatefulSetSpec(
                selector=kclient.V1LabelSelector(),
                service_name="baz",
                template=kclient.V1PodTemplate()
            ),
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )
        rhs = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                },
            },
        }

        self.assertFalse(self.ss._needs_update(lhs, rhs))

    def test__needs_update_returns_true_on_label_change(self):
        lhs = kclient.V1StatefulSet(
            spec=kclient.V1StatefulSetSpec(
                selector=kclient.V1LabelSelector(),
                service_name="baz",
                template=kclient.V1PodTemplate()
            ),
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )
        rhs = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                    "bar": "bar",
                },
            },
        }

        self.assertTrue(self.ss._needs_update(lhs, rhs))

    async def test_rolling_restart_sets_annotation(self):
        ctx = unittest.mock.Mock(["api_client"])
        sts = unittest.mock.Mock(["metadata", "spec"])
        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = sts

            await self.ss.rolling_restart(ctx, sentinel.restart_id)

        get_current.assert_called_once_with(ctx)
        self.ss.resource_interface_mock.patch.assert_called_once_with(
            sts.metadata.namespace,
            sts.metadata.name,
            [
                {
                    "op": "add",
                    "path": (
                        "/spec/template/metadata/annotations"
                        "/kubectl.kubernetes.io~1restartedAt"
                    ),
                    "value": sentinel.restart_id,
                }
            ]
        )

    async def test_adopt_does_nothing_new_if_no_scheduling_keys_are_set(self):
        object_ = {
            "metadata": {},
            "spec": {},
        }
        object_bak = copy.deepcopy(object_)

        with unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object"
                ) as adopt_object:
            await self.ss.adopt_object(
                unittest.mock.sentinel.ctx,
                object_,
            )

        self.assertEqual(object_, object_bak)
        adopt_object.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            object_,
        )

    async def test_adopt_uses_inject_scheduling_keys(self):
        object_ = {
            "spec": {
                "template": {
                    "spec": unittest.mock.sentinel.pod_spec,
                }
            }
        }

        statefulset_state = self.StatefulSetTest(
            component=self.component,
            scheduling_keys=["foo", "bar"],
        )

        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object",
            ))

            inject_scheduling_keys = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.inject_scheduling_keys",
            ))

            await statefulset_state.adopt_object(
                unittest.mock.sentinel.ctx,
                object_,
            )

        adopt_object.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            object_,
        )
        inject_scheduling_keys.assert_called_once_with(
            unittest.mock.sentinel.pod_spec,
            ["foo", "bar"],
        )

    async def test_adopt_autocreates_pod_spec_if_necessary(self):
        object_ = {}

        statefulset_state = self.StatefulSetTest(
            component=self.component,
            scheduling_keys=["foo", "bar"],
        )

        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object",
            ))

            inject_scheduling_keys = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.inject_scheduling_keys",
            ))

            await statefulset_state.adopt_object(
                unittest.mock.sentinel.ctx,
                object_,
            )

        adopt_object.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            object_,
        )
        inject_scheduling_keys.assert_called_once_with(
            object_["spec"]["template"]["spec"],
            ["foo", "bar"],
        )

    async def test_get_used_resources_returns_empty_for_absent_stateful_set(self):  # NOQA
        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_get_current",
            ))
            _get_current.side_effect = exceptions.ResourceNotPresent(
                unittest.mock.sentinel.foo,
                unittest.mock.Mock(),
            )

            result = await self.ss.get_used_resources(self.ctx)

        _get_current.assert_called_once_with(self.ctx)

        self.assertCountEqual(result, [])

    async def test_get_used_resources_extracts_resources_from_stateful_set_pod_spec(self):  # NOQA
        pod_spec = unittest.mock.sentinel.pod_spec

        ss = kclient.V1StatefulSet(spec=kclient.V1StatefulSetSpec(
            service_name="foo",
            selector={},
            template=kclient.V1Pod(spec=pod_spec),
        ))

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_get_current",
            ))
            _get_current.return_value = ss

            extract_pod_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_pod_references",
            ))
            extract_pod_references.return_value = [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ]

            pod_interface_mock = ResourceInterfaceMock()
            pod_interface = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.pod_interface",
            ))
            pod_interface.return_value = pod_interface_mock
            pod_interface_mock.list_.return_value = []

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.LabelSelector.from_api_object",
            ))

            result = await self.ss.get_used_resources(self.ctx)

        extract_pod_references.assert_called_once_with(
            pod_spec,
            self.ctx.namespace,
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ],
        )

    async def test_get_used_resources_extracts_resources_from_existing_pods(self):  # NOQA
        pod_spec = unittest.mock.sentinel.pod_spec

        ss = kclient.V1StatefulSet(spec=kclient.V1StatefulSetSpec(
            service_name="foo",
            selector=unittest.mock.sentinel.selector,
            template=kclient.V1Pod(spec=pod_spec),
        ))

        def generate_resources():
            yield [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ]
            yield [
                unittest.mock.sentinel.r1p1,
                unittest.mock.sentinel.r2p1,
            ]
            yield [
                unittest.mock.sentinel.r1p2,
                unittest.mock.sentinel.r2p2,
            ]
            yield [
                unittest.mock.sentinel.r1p3,
                unittest.mock.sentinel.r2p3,
            ]

        def mock_pod(spec):
            return kclient.V1Pod(spec=spec)

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_get_current",
            ))
            _get_current.return_value = ss

            extract_pod_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_pod_references",
            ))
            extract_pod_references.side_effect = generate_resources()

            pod_interface_mock = ResourceInterfaceMock()
            pod_interface = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.pod_interface",
            ))
            pod_interface.return_value = pod_interface_mock
            pod_interface_mock.list_.return_value = [
                mock_pod(unittest.mock.sentinel.pod1),
                mock_pod(unittest.mock.sentinel.pod2),
                mock_pod(unittest.mock.sentinel.pod3),
            ]

            from_api_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.LabelSelector.from_api_object",
            ))

            result = await self.ss.get_used_resources(self.ctx)

        from_api_object.assert_called_once_with(
            unittest.mock.sentinel.selector,
        )

        pod_interface.assert_called_once_with(
            self.ctx.api_client,
        )

        pod_interface_mock.list_.assert_awaited_once_with(
            self.ctx.namespace,
            label_selector=from_api_object().as_api_selector(),
        )

        self.assertCountEqual(
            extract_pod_references.mock_calls,
            [
                unittest.mock.call(pod_spec, self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod1,
                                   self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod2,
                                   self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod3,
                                   self.ctx.namespace),
            ],
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
                unittest.mock.sentinel.r1p1,
                unittest.mock.sentinel.r2p1,
                unittest.mock.sentinel.r1p2,
                unittest.mock.sentinel.r2p2,
                unittest.mock.sentinel.r1p3,
                unittest.mock.sentinel.r2p3,
            ],
        )

    async def test_is_ready_returns_false_if_nonexistent(self):
        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.side_effect = exceptions.ResourceNotPresent(
                unittest.mock.sentinel.component,
                unittest.mock.Mock(),
            )

            result = await self.ss.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    async def test_is_ready_returns_false_if_no_status(self):
        sts = unittest.mock.Mock(["status"])
        sts.status = None

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = sts

            result = await self.ss.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    async def test_is_ready_returns_true_if_up_to_date(self):
        sts = unittest.mock.Mock(["status"])
        sts.metadata = unittest.mock.Mock([])
        sts.metadata.generation = 123
        sts.status.observed_generation = 123
        sts.status.current_revision = "v1"
        sts.status.update_revision = "v1"
        sts.status.replicas = 3
        sts.status.ready_replicas = 3

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = sts

            result = await self.ss.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertTrue(result)

    @ddt.data(
        # generation, observed_generation, currentrev, updaterev,
        # replicas, ready
        (2, 1, "v1", "v1", 3, 3),
        (2, 2, "v1", "v2", 3, 3),
        (2, 2, "v1", "v2", 3, 2),
        (2, 2, "v2", "v2", 3, 2),
        (2, 2, "v2", "v2", 3, None),
    )
    async def test_is_ready_returns_false_if_not_up_to_date(self, data):
        sts = unittest.mock.Mock(["status"])
        sts.metadata = unittest.mock.Mock([])
        sts.metadata.generation = data[0]
        sts.status.observed_generation = data[1]
        sts.status.current_revision = data[2]
        sts.status.update_revision = data[3]
        sts.status.replicas = data[4]
        sts.status.ready_replicas = data[5]

        with contextlib.ExitStack() as stack:
            get_current = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            get_current.return_value = sts

            result = await self.ss.is_ready(unittest.mock.sentinel.ctx)

        get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        self.assertFalse(result)

    def test_registers_listener_for_deployments(self):
        self.assertCountEqual(
            self.ss.get_listeners(),
            [
                context.KubernetesListener(
                    'apps', 'v1', 'statefulsets',
                    self.ss._handle_sts_event,
                    component=self.component,
                )
            ]
        )

    async def test_handle_sts_event_triggers_reconcile_if_deleted(
            self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.DELETED

        result = self.ss._handle_sts_event(
            self.ctx,
            ev,
        )

        self.assertTrue(result)

    async def test_handle_sts_event_triggers_reconcile_if_readiness_changed_to_true(self):  # noqa:E501
        ev = unittest.mock.Mock([])
        ev.type_ = sentinel.not_deleted
        ev.old_object = sentinel.old
        ev.object_ = sentinel.new

        def is_ready_impl(v):
            # new is ready, old is not
            return v == sentinel.new

        with contextlib.ExitStack() as stack:
            is_ready = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_is_ready",
            ))
            is_ready.side_effect = is_ready_impl

            result = self.ss._handle_sts_event(sentinel.ctx, ev)

        self.assertCountEqual(
            is_ready.mock_calls,
            [
                unittest.mock.call(sentinel.old),
                unittest.mock.call(sentinel.new),
            ]
        )

        self.assertTrue(result)

    @ddt.data(True, False)
    async def test_handle_sts_event_triggers_does_not_trigger_reconcile_if_readiness_unchanged(self, ready_value):  # noqa:E501
        ev = unittest.mock.Mock([])
        ev.type_ = sentinel.not_deleted
        ev.old_object = sentinel.old
        ev.object_ = sentinel.new

        def is_ready_impl(v):
            return ready_value

        with contextlib.ExitStack() as stack:
            is_ready = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_is_ready",
            ))
            is_ready.side_effect = is_ready_impl

            result = self.ss._handle_sts_event(sentinel.ctx, ev)

        self.assertCountEqual(
            is_ready.mock_calls,
            [
                unittest.mock.call(sentinel.old),
                unittest.mock.call(sentinel.new),
            ]
        )

        self.assertFalse(result)

    async def test_handle_sts_event_triggers_does_not_trigger_reconcile_if_became_unready(self):  # noqa:E501
        ev = unittest.mock.Mock([])
        ev.type_ = sentinel.not_deleted
        ev.old_object = sentinel.old
        ev.object_ = sentinel.new

        def is_ready_impl(v):
            # only the old version is unready
            return v == sentinel.old

        with contextlib.ExitStack() as stack:
            is_ready = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_is_ready",
            ))
            is_ready.side_effect = is_ready_impl

            result = self.ss._handle_sts_event(sentinel.ctx, ev)

        self.assertCountEqual(
            is_ready.mock_calls,
            [
                unittest.mock.call(sentinel.old),
                unittest.mock.call(sentinel.new),
            ]
        )

        self.assertFalse(result)

    @ddt.data(
        # readiness of current object, expected reconcile trigger
        (True, True),
        (False, False),
    )
    @ddt.unpack
    async def test_handle_sts_event_treats_absent_old_object_as_unready(
            self,
            new_readiness,
            expected_result,
    ):
        ev = unittest.mock.Mock([])
        ev.type_ = sentinel.not_deleted
        ev.old_object = None
        ev.object_ = sentinel.new

        def is_ready_impl(v):
            return new_readiness

        with contextlib.ExitStack() as stack:
            is_ready = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_is_ready",
            ))
            is_ready.side_effect = is_ready_impl

            result = self.ss._handle_sts_event(sentinel.ctx, ev)

        self.assertCountEqual(
            is_ready.mock_calls,
            [
                unittest.mock.call(sentinel.new),
            ]
        )

        self.assertEqual(expected_result, result)
