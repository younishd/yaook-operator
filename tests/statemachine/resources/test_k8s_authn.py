import unittest

import yaook.statemachine.resources.k8s_authn as k8s_authn

import kubernetes_asyncio.client as kclient

from .utils import ResourceTestMixin


class TestServiceAccount(unittest.TestCase):
    class ServiceAccountTest(ResourceTestMixin, k8s_authn.ServiceAccount):
        pass

    def test__needs_update_returns_true_on_label_change(self):
        sa = self.ServiceAccountTest()
        v1 = kclient.V1ServiceAccount(
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )
        v2 = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "bar"
                }
            }
        }
        self.assertTrue(sa._needs_update(v1, v2))
