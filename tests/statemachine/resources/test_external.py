#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import unittest
import unittest.mock
import uuid
from unittest.mock import sentinel

import yaook.statemachine.interfaces as interfaces

import yaook.statemachine.resources.external as external


class TestExternalResource(unittest.IsolatedAsyncioTestCase):
    class ExternalResourceStateTest(external.ExternalResource):
        def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.update_mock = unittest.mock.AsyncMock()
            self.delete_mock = unittest.mock.AsyncMock()

        async def update(self, ctx, dependencies):
            return await self.update_mock(ctx, dependencies)

        async def delete(self, ctx, dependencies):
            return await self.delete_mock(ctx, dependencies)

    def setUp(self):
        self.finalizer = str(uuid.uuid4())
        self.parent_ri = unittest.mock.Mock(interfaces.ResourceInterface)
        self.ers = self.ExternalResourceStateTest(
            finalizer=self.finalizer,
            component=str(uuid.uuid4()),
        )
        self.ctx = unittest.mock.Mock(["namespace"])
        self.ctx.parent_intf = self.parent_ri
        self.ctx.parent_name = str(uuid.uuid4())
        self.ctx.parent_api_version = "test.yaook.cloud/v1"
        self.ctx.parent_kind = "TestObject"
        self.ctx.field_manager = str(uuid.uuid4())

    async def test_reconcile_without_finalizer_calls_adds_finalizer_and_calls_update(self):  # NOQA
        self.ctx.parent = {}

        def check(*args, **kwargs):
            # we need to ensure that the finalizer is in place when update is
            # called so that we can clean up even if update gets interrupted
            self.parent_ri.patch.assert_awaited()

        await self.ers.reconcile(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )

        self.ers.update_mock.side_effect = check
        self.ers.update_mock.assert_awaited_once_with(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )
        self.ers.delete_mock.assert_not_called()

        self.parent_ri.patch.assert_awaited_once_with(
            self.ctx.namespace,
            self.ctx.parent_name,
            json.dumps(
                {
                    "apiVersion": self.ctx.parent_api_version,
                    "kind": self.ctx.parent_kind,
                    "metadata": {
                        "name": self.ctx.parent_name,
                        "finalizers": [
                            self.finalizer,
                        ],
                    },
                }
            ).encode("utf-8"),
            field_manager=self.finalizer,
        )

    async def test_reconcile_without_finalizer_and_with_deletion_timestamp_does_nothing(self):  # NOQA
        self.ctx.parent = {
            "metadata": {
                "deletionTimestamp": "foo",
            },
        }

        await self.ers.reconcile(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )

        self.ers.update_mock.assert_not_called()
        self.ers.delete_mock.assert_not_called()
        self.parent_ri.patch.assert_not_called()

    async def test_reconcile_with_wrong_finalizer_calls_create_and_adds_finalizer(self):  # NOQA
        self.ctx.parent = {
            "metadata": {
                "finalizers": [
                    "not-the-" + self.finalizer,
                ],
                "deletionTimestamp": None,
            },
        }

        await self.ers.reconcile(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )

        self.ers.update_mock.assert_awaited_once_with(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )
        self.ers.delete_mock.assert_not_called()

        self.parent_ri.patch.assert_awaited_once_with(
            self.ctx.namespace,
            self.ctx.parent_name,
            json.dumps(
                {
                    "apiVersion": self.ctx.parent_api_version,
                    "kind": self.ctx.parent_kind,
                    "metadata": {
                        "name": self.ctx.parent_name,
                        "finalizers": [
                            self.finalizer,
                        ],
                    },
                }
            ).encode("utf-8"),
            field_manager=self.finalizer,
        )

    async def test_reconcile_with_finalizer_calls_update_and_does_not_modify_resource(self):  # NOQA
        self.ctx.parent = {
            "metadata": {
                "finalizers": [
                    self.finalizer,
                ],
            },
        }

        await self.ers.reconcile(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )

        self.ers.update_mock.assert_awaited_once_with(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )
        self.ers.delete_mock.assert_not_called()
        self.parent_ri.patch.assert_not_called()

    async def test_reconcile_with_non_null_deletion_timestamp_calls_delete_and_removes_finalizer(self):  # NOQA
        self.ctx.parent = {
            "metadata": {
                "finalizers": [
                    "foo",
                    self.finalizer,
                    "baz",
                ],
                "deletionTimestamp": "foo",
            },
        }

        await self.ers.reconcile(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )

        self.ers.update_mock.assert_not_called()
        self.ers.delete_mock.assert_awaited_once_with(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )

        self.parent_ri.patch.assert_awaited_once_with(
            self.ctx.namespace,
            self.ctx.parent_name,
            json.dumps(
                {
                    "apiVersion": self.ctx.parent_api_version,
                    "kind": self.ctx.parent_kind,
                    "metadata": {
                        "name": self.ctx.parent_name,
                        "finalizers": [],
                    },
                }
            ).encode("utf-8"),
            field_manager=self.finalizer,
        )

    async def test_reconcile_keeps_finalizer_and_propagates_error_if_create_fails(self):  # NOQA
        class FooException(Exception):
            pass

        self.ctx.parent = {}

        self.ers.update_mock.side_effect = FooException()

        with self.assertRaises(FooException):
            await self.ers.reconcile(
                self.ctx,
                unittest.mock.sentinel.dependencies,
            )

        self.ers.update_mock.assert_awaited_once_with(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )
        self.ers.delete_mock.assert_not_called()

        self.parent_ri.patch.assert_awaited_once_with(
            self.ctx.namespace,
            self.ctx.parent_name,
            json.dumps(
                {
                    "apiVersion": self.ctx.parent_api_version,
                    "kind": self.ctx.parent_kind,
                    "metadata": {
                        "name": self.ctx.parent_name,
                        "finalizers": [
                            self.finalizer,
                        ],
                    },
                }
            ).encode("utf-8"),
            field_manager=self.finalizer,
        )

    async def test_reconcile_keeps_finalizer_and_propagates_error_if_delete_fails(self):  # NOQA
        class FooException(Exception):
            pass

        self.ctx.parent = {
            "metadata": {
                "finalizers": [
                    "foo",
                    self.finalizer,
                    "baz",
                ],
                "deletionTimestamp": "foo",
            },
        }

        self.ers.delete_mock.side_effect = FooException()

        with self.assertRaises(FooException):
            await self.ers.reconcile(
                self.ctx,
                unittest.mock.sentinel.dependencies,
            )

        self.ers.delete_mock.assert_awaited_once_with(
            self.ctx,
            unittest.mock.sentinel.dependencies,
        )

        self.parent_ri.patch.assert_not_called()

    async def test_cleanup_orphans_is_most_likely_noop(self):
        await self.ers.cleanup_orphans(sentinel.ctx, sentinel.protect)
