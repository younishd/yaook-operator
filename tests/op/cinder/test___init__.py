#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import unittest
import unittest.mock

import ddt
import kubernetes_asyncio.client as kclient

import yaook.common.config
import yaook.statemachine as sm
import yaook.op.cinder as cinder


CEPH_BACKEND_1 = {
                    "rbd": {
                        "keyringReference": "cinder-client-key",
                        "keyringUsername": "cinder"
                    }
                }
NETAPP_BACKEND_1 = {
                        "netapp": {
                            "login": "username",
                            "passwordReference": "password",
                            "server": "server",
                            "vserver": "vserver1",
                            "shares": ["backend1_share1"]
                        }
                    }

NETAPP_BACKEND_2 = {
                        "netapp": {
                            "login": "username",
                            "passwordReference": "password",
                            "server": "server",
                            "vserver": "vserver2",
                            "shares": ["backend2_share1", "backend2_share2"]
                        }
                    }


class TestBackupSpecAccessor(unittest.IsolatedAsyncioTestCase):
    async def test_returns_backup_spec(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {"backup": {"key1": 123, "key2": 653}}
        self.assertEqual(
            await cinder._backup_spec_accessor(ctx),
            {"key1", "key2"}
        )


class TestBackendSpecAccessor(unittest.IsolatedAsyncioTestCase):
    async def test_returns_backend_spec(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {"backends": {"test1": {}, "test2": {}}}
        self.assertEqual(
            await cinder._backend_spec_accessor(ctx),
            {"test1", "test2"}
        )


class TestCephLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.cl = cinder.CephLayer()

    def test_is_cue_layer(self):
        self.assertIsInstance(self.cl, sm.CueLayer)

    async def test_returns_empty_if_ceph_disabled(self):
        ctx = unittest.mock.Mock(["parent_spec", "instance"])
        ctx.parent_spec = {"backends": {}}
        ctx.instance = None

        self.assertDictEqual(await self.cl.get_layer(ctx), {})

    async def test_injects_ceph_config_in_cue(self):
        ctx = unittest.mock.Mock(["parent_spec", "instance"])
        ctx.instance = "cephbackend"
        ctx.parent_spec = {
            "backends": {
                "cephbackend": {
                    "rbd": {
                        "keyringReference": "cinder-client-key",
                        "keyringUsername": "cinder",
                        "backendConfig": {
                            "expiry_thres_minutes": "2880",
                            "rbd_pool": "cinder-pool"
                        },
                    }
                }
            }

        }

        result = await self.cl.get_layer(ctx)

        self.assertIsInstance(
            result["cinder"].contents[0]["cephbackend"],
            yaook.common.config.CueConfigReference
        )

    async def test_multiple_backends_defined(self):
        self.cl = cinder.CephLayer()
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.instance = "cephbackend2"
        ctx.parent_spec = {
            "backends": {
                "cephbackend1": {
                    "rbd": {
                        "keyringReference": "cinder-client-key",
                        "keyringUsername": "cinder",
                        "backendConfig": {
                            "expiry_thres_minutes": "2880",
                            "rbd_pool": "cinder-pool"
                        },
                    }
                },
                "cephbackend2": {
                    "rbd": {
                        "keyringReference": "cinder-client-key",
                        "keyringUsername": "cinder",
                        "backendConfig": {
                            "expiry_thres_minutes": "2880",
                            "rbd_pool": "cinder-pool"
                        },
                    }
                }
            },
        }

        result = await self.cl.get_layer(ctx)

        self.assertEqual(len(result["cinder"].contents), 3)
        for config in result["cinder"].contents:
            self.assertTrue("cephbackend2" in config.keys())


class TestEnabledBackendsLayer(unittest.IsolatedAsyncioTestCase):
    def test_is_cue_layer(self):
        ebl = cinder.EnabledBackendLayer()
        self.assertIsInstance(ebl, sm.CueLayer)

    async def test_instance_is_returned(self):
        ebl = cinder.EnabledBackendLayer()
        ctx = unittest.mock.Mock(["api_client", "namespace", "parent_spec",
                                  "instance"])
        ctx.instance = "test1"
        ctx.parent_spec = {
            "backends": {
                "test1": {}
            }
        }

        result = await ebl.get_layer(ctx)
        self.assertEqual(result["cinder"].contents[0]["DEFAULT"]
                         ["enabled_backends"], ["test1"])


class TestNetappLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.nl = cinder.NetappLayer()

    def test_is_cue_layer(self):
        self.assertIsInstance(self.nl, sm.CueLayer)

    async def test_returns_empty_if_netapp_disabled(self):
        ctx = unittest.mock.Mock(["api_client", "namespace", "parent_spec",
                                  "instance"])
        ctx.instance = "not_netapp"
        ctx.parent_spec = {"backends": {"not_netapp": {}}}

        self.assertDictEqual(await self.nl.get_layer(ctx), {})

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.ResourceInterface.read")
    async def test_injects_netapp_config_in_cue(self, read):
        secretdata = sm.api_utils.encode_secret_data({
            "password": "hello"
        })
        read.return_value = kclient.V1Secret(data=secretdata)
        ctx = unittest.mock.Mock(["api_client", "namespace", "parent_spec",
                                  "instance"])
        ctx.instance = "netappbackend"
        ctx.parent_spec = {
            "backends": {
                "netappbackend": {
                    "netapp": {
                        "login": "username",
                        "passwordReference": "password",
                        "server": "server",
                        "vserver": "vserver",
                        "shares": ["someshare"],
                        "backendConfig": {
                            "expiry_thres_minutes": "2880",
                            "backendHost": "hostname",
                        }
                    }
                }
            }
        }

        result = await self.nl.get_layer(ctx)

        self.assertIsInstance(
            result["cinder"].contents[0]["netappbackend"],
            yaook.common.config.CueConfigReference
        )
        self.assertEqual(
            "hello",
            result["cinder"].contents[1]["netappbackend"]["netapp_password"]
        )
        self.assertEqual(
            "hostname",
            result["cinder"].contents[2]["netappbackend"]
            .get("backendHost")
        )
        self.assertEqual(
            None,
            result["cinder"].contents[-1]["netappbackend"]
            .get("netapp_copyoffload_tool_path")
        )

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.ResourceInterface.read")
    async def test_netapp_copyoffload_config(self, read):
        secretdata = sm.api_utils.encode_secret_data({
            "password": "hello"
        })
        read.return_value = kclient.V1Secret(data=secretdata)
        ctx = unittest.mock.Mock(["api_client", "namespace", "parent_spec",
                                  "instance"])
        ctx.instance = "netappbackend"
        ctx.parent_spec = {
            "backends": {
                "netappbackend": {
                    "netapp": {
                        "login": "username",
                        "passwordReference": "password",
                        "server": "server",
                        "vserver": "vserver",
                        "shares": ["someshare"],
                        "copyoffloadConfigMap": "copyoffloadConfigMap",
                    }
                }
            }
        }

        result = await self.nl.get_layer(ctx)

        self.assertIsInstance(
            result["cinder"].contents[0]["netappbackend"],
            yaook.common.config.CueConfigReference
        )
        self.assertEqual(
            "/na_copyoffload/na_copyoffload_64",
            result["cinder"].contents[2]["netappbackend"]
            ["netapp_copyoffload_tool_path"]
        )

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.ResourceInterface.read")
    async def test_netapp_copyoffload_multiple_backends_only_one(self, read):
        secretdata = sm.api_utils.encode_secret_data({
            "password": "hello"
        })
        read.return_value = kclient.V1Secret(data=secretdata)
        ctx = unittest.mock.Mock(["api_client", "namespace", "parent_spec",
                                  "instance"])
        ctx.parent_spec = {
            "backends": {
                "netappbackend1": {
                    "netapp": {
                        "login": "username",
                        "passwordReference": "password",
                        "server": "server",
                        "vserver": "vserver",
                        "shares": ["someshare"],
                        "copyoffloadConfigMap": "copyoffloadConfigMap",
                    }
                },

                "netappbackend2": {
                    "netapp": {
                        "login": "username",
                        "passwordReference": "password",
                        "server": "server",
                        "vserver": "vserver",
                        "shares": ["someshare"],
                    }
                }
            }
        }

        ctx.instance = "netappbackend1"
        result = await self.nl.get_layer(ctx)

        self.assertIsInstance(
            result["cinder"].contents[0]["netappbackend1"],
            yaook.common.config.CueConfigReference
        )

        self.assertEqual(
            "/na_copyoffload/na_copyoffload_64",
            result["cinder"].contents[2]["netappbackend1"]
            ["netapp_copyoffload_tool_path"]
        )

        ctx.instance = "netappbackend2"
        result = await self.nl.get_layer(ctx)

        self.assertIsInstance(
            result["cinder"].contents[0]["netappbackend2"],
            yaook.common.config.CueConfigReference
        )
        self.assertEqual(
            2,
            len(result["cinder"].contents)
        )

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.ResourceInterface.read")
    async def test_netapp_copyoffload_multiple_backends_all(self, read):
        secretdata = sm.api_utils.encode_secret_data({
            "password": "hello"
        })
        read.return_value = kclient.V1Secret(data=secretdata)
        ctx = unittest.mock.Mock(["api_client", "namespace", "parent_spec",
                                  "instance"])
        ctx.instance = "netappbackend1"
        ctx.parent_spec = {
            "backends": {
                "netappbackend1": {
                    "netapp": {
                        "login": "username",
                        "passwordReference": "password",
                        "server": "server",
                        "vserver": "vserver",
                        "shares": ["someshare"],
                        "copyoffloadConfigMap": "copyoffloadConfigMap",
                    }
                },

                "netappbackend2": {
                    "netapp": {
                        "login": "username",
                        "passwordReference": "password",
                        "server": "server",
                        "vserver": "vserver",
                        "shares": ["someshare"],
                        "copyoffloadConfigMap": "copyoffloadConfigMap",
                    }
                }
            }
        }

        result = await self.nl.get_layer(ctx)

        self.assertIsInstance(
            result["cinder"].contents[0]["netappbackend1"],
            yaook.common.config.CueConfigReference
        )
        self.assertEqual(
            "/na_copyoffload/na_copyoffload_64",
            result["cinder"].contents[2]["netappbackend1"]
            ["netapp_copyoffload_tool_path"]
        )

        ctx.instance = "netappbackend2"
        result = await self.nl.get_layer(ctx)

        self.assertIsInstance(
            result["cinder"].contents[0]["netappbackend2"],
            yaook.common.config.CueConfigReference
        )
        self.assertEqual(
            "/na_copyoffload/na_copyoffload_64",
            result["cinder"].contents[2]["netappbackend2"]
            .get("netapp_copyoffload_tool_path")
        )

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.ResourceInterface.read")
    async def test_multiple_bacckends_defined(self, read):
        self.nl = cinder.NetappLayer()
        secretdata = sm.api_utils.encode_secret_data({
            "password": "hello"
        })
        read.return_value = kclient.V1Secret(data=secretdata)
        ctx = unittest.mock.Mock(["api_client", "namespace", "parent_spec",
                                  "instance"])
        ctx.instance = "netappbackend2"
        ctx.parent_spec = {
            "backends": {
                "netappbackend1": {
                    "netapp": {
                        "login": "username",
                        "passwordReference": "password",
                        "server": "server",
                        "vserver": "vserver",
                        "shares": ["someshare"],
                    }
                },

                "netappbackend2": {
                    "netapp": {
                        "login": "username",
                        "passwordReference": "password",
                        "server": "server",
                        "vserver": "vserver",
                        "shares": ["someshare"],
                    }
                }
            }
        }

        result = await self.nl.get_layer(ctx)

        self.assertEqual(2, len(result["cinder"].contents))
        self.assertIsInstance(
            result["cinder"].contents[0]["netappbackend2"],
            yaook.common.config.CueConfigReference
        )
        self.assertEqual(
            "username",
            result["cinder"].contents[1]["netappbackend2"]
            .get("netapp_login")
        )


class TestDeployment(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.sts = cinder.Deployment(
            template="",
            component="test",
            scheduling_keys=["TEST", "TEST1"]
        )

    @unittest.mock.patch(
        "yaook.statemachine.resources.BodyTemplateMixin."
        "_get_template_parameters"
    )
    async def test_invalid_copyoffload_configuration(
            self, _get_template_parameters):

        _get_template_parameters.return_value = {}

        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends": {
                "netappbackend": {
                    "netapp": {
                        "login": "username",
                        "passwordReference": "password",
                        "server": "server",
                        "vserver": "vserver",
                        "copyoffloadConfigMap": {
                            "name": "copyoffloadConfigMap",
                        },
                        "shares": ["someshare"]
                    }},
                "netappbackend2": {
                    "netapp": {
                        "login": "username",
                        "passwordReference": "password",
                        "server": "server",
                        "vserver": "vserver",
                        "copyoffloadConfigMap": {
                            "name": "copyoffloadConfigMap2",
                        },
                        "shares": ["someshare"]
                    }}
            }
        }

        with self.assertRaises(sm.ConfigurationInvalid):
            await self.sts._get_template_parameters(ctx, {})


@ddt.ddt
class TestConfig(unittest.IsolatedAsyncioTestCase):

    def setUp(self):
        self.cs = cinder.Config(metadata=("cinder-config-", True),)

    @ddt.data(
        ({"backends": {"ceph1": CEPH_BACKEND_1}}, {}),
        ({"backends": {"netapp1": NETAPP_BACKEND_1}},
            {"nfs_shares_vserver1": "backend1_share1"}),
        ({"backends": {"netapp1": NETAPP_BACKEND_1,
                       "netapp2": NETAPP_BACKEND_2}},
            {"nfs_shares_vserver1": "backend1_share1",
             "nfs_shares_vserver2": "backend2_share1\nbackend2_share2"}),
        ({"backends": {"ceph1": CEPH_BACKEND_1, "netapp1": NETAPP_BACKEND_1,
                       "netapp2": NETAPP_BACKEND_2}},
            {"nfs_shares_vserver1": "backend1_share1",
             "nfs_shares_vserver2": "backend2_share1\nbackend2_share2"}),
    )
    @ddt.unpack
    async def test_injects_shares(self, data, expected):
        ctx = unittest.mock.Mock(["api_client", "namespace", "parent_spec",
                                  "instance"])
        result = {}
        ctx.parent_spec = data
        for backend in data["backends"].keys():
            ctx.instance = backend
            result.update((await self.cs._make_body(ctx, dependencies={}))
                          .get("data", {}))

        for key in expected.keys():
            self.assertIn(key, result)
            self.assertEqual(
                expected[key],
                base64.b64decode(result[key]).decode('ascii'))
