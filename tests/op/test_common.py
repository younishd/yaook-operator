#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
import unittest.mock

import yaook.op.common as common


class Testextract_labeled_configs(unittest.TestCase):
    def test_returns_configs_which_match_given_labelset(self):
        item1 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                },
            }],
            "instance": 1,
        }
        item2 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v2",
                },
            }],
        }
        item3 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l2": "v1",
                },
            }],
        }
        item4 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                    "l2": "v1",
                },
            }],
        }
        item5 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                },
            }],
            "instance": 2,
        }

        input_ = [item1, item2, item3, item4, item5]

        result = common.extract_labeled_configs(
            input_,
            {
                "l1": "v1",
            }
        )

        self.assertCountEqual(
            result,
            [item1, item5],
        )

    def test_returns_configs_with_union_of_selectors(self):
        item1 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                },
            }],
            "instance": 1,
        }
        item2 = {
            "nodeSelectors": [
                {
                    "matchLabels": {
                        "l1": "v2",
                    },
                },
                {
                    "matchLabels": {
                        "l1": "v1",
                    },
                }
            ],
        }
        item3 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l2": "v1",
                },
            }],
        }
        item4 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                    "l2": "v1",
                },
            }],
        }
        item5 = {
            "nodeSelectors": [{
                "matchLabels": {
                    "l1": "v1",
                },
            }],
            "instance": 2,
        }

        input_ = [item1, item2, item3, item4, item5]

        result = common.extract_labeled_configs(
            input_,
            {
                "l1": "v1",
            }
        )

        self.assertCountEqual(
            result,
            [item1, item2, item5],
        )


class Testtranspose_config(unittest.TestCase):
    def test_collects_subkeys_in_lists(self):
        input_ = [
            {
                "key1": unittest.mock.sentinel.key1_1,
            },
            {
                "key2": unittest.mock.sentinel.key2_1,
            },
            {
                "key1": unittest.mock.sentinel.key1_2,
                "key2": unittest.mock.sentinel.key2_2,
                "key3": unittest.mock.sentinel.ignored,
            },
            {
                "key3": unittest.mock.sentinel.ignored,
            },
        ]

        result = common.transpose_config(
            input_,
            ["key1", "key2"],
        )

        self.assertEqual(
            result,
            {
                "key1": [
                    unittest.mock.sentinel.key1_1,
                    unittest.mock.sentinel.key1_2,
                ],
                "key2": [
                    unittest.mock.sentinel.key2_1,
                    unittest.mock.sentinel.key2_2,
                ],
            }
        )

    def test_contains_empty_list_for_nonexistent_key(self):
        input_ = [
            {
                "key1": unittest.mock.sentinel.key1_1,
            },
        ]

        result = common.transpose_config(
            input_,
            ["key4"],
        )

        self.assertEqual(
            result,
            {
                "key4": [],
            }
        )


class Testget_node_labels_from_instance(unittest.IsolatedAsyncioTestCase):
    @unittest.mock.patch("kubernetes_asyncio.client.CoreV1Api")
    async def test_extracts_node_labels_from_api(
            self,
            CoreV1Api):
        node = unittest.mock.Mock(["metadata"])
        node.metadata = unittest.mock.Mock(["labels"])
        ctx = unittest.mock.Mock(["api_client", "instance"])
        v1 = unittest.mock.Mock(["read_node"])
        v1.read_node = unittest.mock.AsyncMock()
        v1.read_node.return_value = node
        CoreV1Api.return_value = v1

        result = await common.get_node_labels_from_instance(ctx)

        self.assertEqual(result, node.metadata.labels)

    @unittest.mock.patch("kubernetes_asyncio.client.CoreV1Api")
    async def test_uses_api_client_from_context(
            self,
            CoreV1Api):
        node = unittest.mock.Mock(["metadata"])
        node.metadata = unittest.mock.Mock(["labels"])
        ctx = unittest.mock.Mock(["api_client", "instance"])
        v1 = unittest.mock.Mock(["read_node"])
        v1.read_node = unittest.mock.AsyncMock()
        v1.read_node.return_value = node
        CoreV1Api.return_value = v1

        await common.get_node_labels_from_instance(ctx)

        CoreV1Api.assert_called_once_with(ctx.api_client)

    @unittest.mock.patch("kubernetes_asyncio.client.CoreV1Api")
    async def test_uses_instance_as_node_name(
            self,
            CoreV1Api):
        node = unittest.mock.Mock(["metadata"])
        node.metadata = unittest.mock.Mock(["labels"])
        ctx = unittest.mock.Mock(["api_client", "instance"])
        v1 = unittest.mock.Mock(["read_node"])
        v1.read_node = unittest.mock.AsyncMock()
        v1.read_node.return_value = node
        CoreV1Api.return_value = v1

        await common.get_node_labels_from_instance(ctx)

        v1.read_node.assert_called_once_with(ctx.instance)


class Testpublish_endpoint(unittest.TestCase):
    async def test_publish_endpoint_defaults_to_true(
            self):
        self.assertTrue(common.publish_endpoint())

    async def test_publish_endpoint_returns_false(
            self):
        ctx = {
            "parent_spec": {
                "api": {
                    "publishEndpoint": False
                }
            }
        }

        return_value = common.publish_endpoint(ctx)
        self.assertFalse(return_value)
