// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#memcached
crd.#region
crd.#issuer
{
	#kind:     "NovaDeployment"
	#plural:   "novadeployments"
	#singular: "novadeployment"
	#shortnames: ["novad", "novads"]
	#releases: ["queens", "train", "yoga"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"api",
			"conductor",
			"metadata",
			"scheduler",
			"vnc",
			"placement",
			"targetRelease",
			"novaConfig",
			"database",
			"messageQueue",
			"memcached",
			"region",
			"issuerRef",
			"databaseCleanup",
			"eviction",
		]
		properties: {
			keystoneRef:    crd.#keystoneref
			serviceMonitor: crd.#servicemonitor

			database: {
				type:        "object"
				description: "Database deployment configurations. Nova and placement require four different databases."
				required: ["api", "cell0", "cell1", "placement"]
				properties: {
					api:       crd.#databasesnip
					placement: crd.#databasesnip
					cell0:     crd.#databasesnip
					cell1:     crd.#databasesnip
				}
			}

			databaseCleanup: {
				type:        "object"
				description: "Configuration of a periodic database cleanup job, using nova-manage."
				required: ["schedule"]
				properties: {
					schedule: crd.#cronschedulesnip
					deletionTimeRange: {
						type:        "integer"
						description: "Deleted database rows older than this number of days will be removed."
						default:     60
					}
				}
			}

			messageQueue: {
				type:        "object"
				description: "Message queue configurations."
				required: ["cell1"]
				properties: cell1: crd.#messagequeuesnip
			}

			api:         crd.apiendpoint
			conductor:   crd.replicated
			placement:   crd.apiendpoint
			scheduler:   crd.replicated
			consoleauth: crd.replicated
			vnc:         crd.apiendpoint
			metadata:    crd.replicated

			novaConfig: {
				crd.#anyconfig
				#description: "Nova OpenStack config. "
			}
			novaSecrets: crd.#configsecret
			policy: {
				type: "object"
				additionalProperties: type: "string"
			}

			placementConfig: {
				crd.#anyconfig
				#description: "Placement OpenStack config. "
			}
			placementSecrets: crd.#configsecret
			placementPolicy: {
				type: "object"
				additionalProperties: type: "string"
			}

			compute: {
				crd.pernodeconfig
				properties: configTemplates: items: properties: {
					novaComputeConfig: {
						crd.#anyconfig
						#description: "Nova Compute service config. "
					}
					hostAggregates: {
						type:        "array"
						description: "NovaHostAggregate resource names, identifying host aggregates this compute node should be part of. These lists are joined if multiple configTemplates match a node."
						items: {
							type: "string"
						}
					}
					volumeBackends: {
						type:        "object"
						description: "Optional configuration of a Ceph RBD volume backend."
						properties: ceph: {
							type:        "object"
							description: "Ceph RBD backend configuration"
							required: ["enabled", "keyringSecretName", "user", "uuid"]
							properties: {
								enabled: {
									type:        "boolean"
									description: "Whether to enable the backend"
								}
								keyringSecretName: {
									type:        "string"
									description: "Name of the Kubernetes secret containing the Ceph keyring to use. The secret must be in ``kubernetes.io/rook`` format."
								}
								user: {
									type:        "string"
									description: "RADOS username to use for authentication."
								}
								uuid: {
									type:        "string"
									pattern:     "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}"
									description: "libvirt Secret UUID to store the keyring secret in. This must match the rbd_secret_uuid in Cinder to be able to use volumes."
								}
								cephConfig: {
									crd.#anyconfig
									#description: "Extra Ceph configuration to inject. This is, in contrast to the Cinder ceph configuration, on the top level, i.e. not specific to the RADOS user referenced above."
								}
							}
						}
					}
				}
			}

			eviction: crd.eviction

			ids: {
				type: "object"
				properties: {
					cinderGid: type: "integer"
				}
			}

			api: {
				description: "Nova API deployment configuration"
				properties: resources: {
					type:        "object"
					description: "Resource requests/limits related to containers in Nova API pods"
					properties: {
						"nova-api":                crd.#containerresources
						"ssl-terminator":          crd.#containerresources
						"ssl-terminator-external": crd.#containerresources
						"ssl-terminator-internal": crd.#containerresources
						"service-reload":          crd.#containerresources
						"service-reload-external": crd.#containerresources
						"service-reload-internal": crd.#containerresources
					}
				}
			}
			metadata: {
				description: "Nova Metadata service deployment configuration"
				properties: resources: {
					type:        "object"
					description: "Resource requests/limits related to containers in Nova Metadata service pods"
					properties: {
						"nova-metadata":  crd.#containerresources
						"ssl-terminator": crd.#containerresources
						"service-reload": crd.#containerresources
					}
				}
			}
			placement: {
				description: "Placement API deployment configuration"
				properties: resources: {
					type:        "object"
					description: "Resource requests/limits related to containers in Placement API pods"
					properties: {
						"placement":               crd.#containerresources
						"ssl-terminator":          crd.#containerresources
						"ssl-terminator-external": crd.#containerresources
						"ssl-terminator-internal": crd.#containerresources
						"service-reload":          crd.#containerresources
						"service-reload-external": crd.#containerresources
						"service-reload-internal": crd.#containerresources
					}
				}
			}
			vnc: {
				description: "VNC proxy service deployment configuration"
				properties: resources: {
					type:        "object"
					description: "Resource requests/limits related to containers in VNC proxy service pods"
					properties: {
						"nova-novncproxy":         crd.#containerresources
						"ssl-terminator-external": crd.#containerresources
						"service-reload-external": crd.#containerresources
					}
				}
			}
			conductor: {
				description: "Nova Conductor deployment configuration"
				properties: resources: {
					type:        "object"
					description: "Resource requests/limits related to containers in Nova Conductor pods"
					properties: "nova-conductor": crd.#containerresources
				}
			}
			consoleauth: {
				description: "Nova Consoleauth service deployment configuration"
				properties: resources: {
					type:        "object"
					description: "Resource requests/limits related to containers in Nova Consoleauth service pods"
					properties: "nova-consoleauth": crd.#containerresources
				}
			}
			scheduler: {
				description: "Nova Scheduler deployment configuration"
				properties: resources: {
					type:        "object"
					description: "Resource requests/limits related to containers in Nova Scheduler pods"
					properties: "nova-scheduler": crd.#containerresources
				}
			}
			compute: properties: resources: {
				type:        "object"
				description: "Resource requests/limits related to containers in Nova Compute pods"
				properties: {
					"keygen":            crd.#containerresources
					"chown-nova":        crd.#containerresources
					"nova-compute":      crd.#containerresources
					"nova-compute-ssh":  crd.#containerresources
					"libvirtd":          crd.#containerresources
					"compute-evict-job": crd.#containerresources
				}
			}
			evictPollMigrationSpeedLocalDisk: {
				type:        "integer"
				default:     30
				description: "The speed in MiB/s to what the migration of vms will be temporarily limited, when the root disk is local and using poll migration during the eviction process. Used 30 MiB/s as default, based on experience."
			}
			jobResources: {
				type:        "object"
				description: "Resource limits for Job Pod containers spawned by the Operator"
				properties: {
					"nova-db-cleanup-cronjob": crd.#containerresources
					"nova-api-db-sync-job":    crd.#containerresources
					"nova-create-cell1-job":   crd.#containerresources
					"nova-db-sync-job":        crd.#containerresources
					"nova-map-cell0-job":      crd.#containerresources
					"placement-db-sync-job":   crd.#containerresources
				}
			}
		}
	}
}
