// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#memcached
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "NeutronDeployment"
	#plural:   "neutrondeployments"
	#singular: "neutrondeployment"
	#shortnames: ["neutrond", "neutronds"]
	#releases: ["queens", "train", "yoga"]
	#schema: properties: spec: {
		required: [
			"novaRef",
			"keystoneRef",
			"api",
			"targetRelease",
			"neutronConfig",
			"neutronML2Config",
			"setup",
			"database",
			"messageQueue",
			"memcached",
			"region",
			"issuerRef",
		]
		properties: {
			novaRef:     crd.#ref
			keystoneRef: crd.#keystoneref

			api:            crd.apiendpoint
			serviceMonitor: crd.#servicemonitor
			neutronConfig: {
				crd.#anyconfig
				#description: "Neutron OpenStack config. "
			}
			neutronSecrets: crd.#configsecret
			neutronML2Config: {
				crd.#anyconfig
				#description: "Neutron ML2 Plugin config. "
			}
			messageQueue: crd.#messagequeuesnip

			setup: {
				type:        "object"
				description: "The type of neutron setup you want. You can choose between 'ovs' and 'ovn'."
				oneOf: [
					{required: ["ovs"]},
					{required: ["ovn"]},
				]
				properties: {
					ovn: {
						type:        "object"
						description: "OVN-based deployment."
						required: [
							"controller",
							"northboundOVSDB",
							"northd",
							"southboundOVSDB",
						]
						properties: {
							controller: {
								crd.pernodeconfig
								description: "Per-node configuration for the OVN controller services."
								properties: {
									configTemplates: items: properties: {
										neutronMetadataAgentConfig: {
											crd.#anyconfig
											#description: "Neutron Metadata Agent config. "
										}
										ovnMonitorAll: {
											type:        "boolean"
											description: "Monitor everything in the ovs-database. Setting this to `true` will reduce the load on the north- and southbound database while increasing memory consumption of ovn-controllers."
										}
										bridgeConfig: {
											type:        "array"
											description: "Map physical devices to OpenStack provider physical network names."
											items: {
												type: "object"
												required: ["bridgeName", "uplinkDevice", "openstackPhysicalNetwork"]
												properties: {
													bridgeName: {
														type:        "string"
														description: "Unique name for the bridge to plug the uplinkDevice into. Must not exist already on the host."
													}
													uplinkDevice: {
														type:        "string"
														description: "Name of the physical network interface to expose to OpenStack. Must exist already on the host and may be a VLAN device (or any other interface type, really)."
													}
													openstackPhysicalNetwork: {
														type:        "string"
														description: "Name under which this network is exposed in openstack, as ``--provider-physical-network`` in openstack network create."
													}
												}
											}
										}
										scrapeIntervalMs: {
											type:        "integer"
											description: "time in milliseconds the metrics of the local ovsdb server is scraped"
										}
									}
									resources: {
										type:        "object"
										description: "Container resource requests/limits for OVN-controller related containers"
										properties: {
											"ovsdb-server":            crd.#containerresources
											"ovs-vswitchd":            crd.#containerresources
											"ovs-vswitchd-monitoring": crd.#containerresources
											"ssl-terminator":          crd.#containerresources
											"service-reload":          crd.#containerresources
										}
									}
								}
							}
							northboundOVSDB: {
								type:        "object"
								description: "Northbound OpenvSwitch Database deployment configuration"
								crd.replicated
								crd.storageconfig
								required: ["backup"]
								properties: {
									backup: crd.backupspec
									inactivityProbeMs: {
										type:        "integer"
										description: "time in milliseconds till ovsdb server see connection as inactive"
									}
									scrapeIntervalMs: {
										type:        "integer"
										description: "time in milliseconds the metrics of the ovsdb server is scraped"
									}
									ovnRelay: {
										type:        "object"
										description: "Configure and use an OVN relay (optional)"
										crd.replicated
										properties: {
											resources: {
												type:        "object"
												description: "Container resource requests/limits for the relay containers"
												properties: {
													"ovn-relay":       crd.#containerresources
													"ssl-terminator":  crd.#containerresources
													"service-reload'": crd.#containerresources
												}
											}
										}
									}
									resources: {
										type:        "object"
										description: "Container resource requests/limits for northbound OVSDB containers"
										properties: {
											"setup-ovsdb":     crd.#containerresources
											"ovsdb":           crd.#containerresources
											"backup-creator":  crd.#containerresources
											"backup-shifter":  crd.#containerresources
											"ssl-terminator":  crd.#containerresources
											"service-reload'": crd.#containerresources
										}
									}
								}
							}
							southboundOVSDB: {
								type:        "object"
								description: "Southbound OpenvSwitch Database deployment configuration"
								crd.replicated
								crd.storageconfig
								required: ["backup", "ovnRelay"]
								properties: {
									backup: crd.backupspec
									inactivityProbeMs: {
										type:        "integer"
										description: "time in milliseconds till ovsdb server see connection as inactive"
									}
									scrapeIntervalMs: {
										type:        "integer"
										description: "time in milliseconds the metrics of the ovsdb server is scraped"
									}
									ovnRelay: {
										type:        "object"
										description: "Configure and use an OVN relay (optional). This is userfull for larger OVN environments. A good first guess would be one replica per 25 compute nodes you have."
										crd.replicated
										properties: {
											resources: {
												type:        "object"
												description: "Container resource requests/limits for the relay containers"
												properties: {
													"ovn-relay":       crd.#containerresources
													"ssl-terminator":  crd.#containerresources
													"service-reload'": crd.#containerresources
												}
											}
										}
									}
									resources: {
										type:        "object"
										description: "Container resource requests/limits for southbound OVSDB containers"
										properties: {
											"setup-ovsdb":     crd.#containerresources
											"ovsdb":           crd.#containerresources
											"backup-creator":  crd.#containerresources
											"backup-shifter":  crd.#containerresources
											"ssl-terminator":  crd.#containerresources
											"service-reload'": crd.#containerresources
										}
									}
								}
							}
							northd: {
								type:        "object"
								description: "northd deployment configuration"
								crd.replicated
								properties: {
									resources: {
										type:        "object"
										description: "Container resource requests/limits for northd containers"
										properties: {
											"northd": crd.#containerresources
										}
									}
								}
							}
							bgp: {
								type: "object"
								additionalProperties: {
									type: "object"
									crd.pernodeconfig
									properties: {
										configTemplates: items: properties: {
											config: {
												type: "object"
												required: ["bridgeName", "driver", "localAS", "peers"]
												crd.ovnBgpConfig
											}
										}
									}
								}
							}
						}
					}
					ovs: {
						type: "object"
						required: [
							"l2",
							"l3",
							"dhcp",
						]
						properties: {
							l2: {
								crd.pernodeconfig
								properties: {
									configTemplates: items: properties: {
										neutronConfig:                 crd.#anyconfig
										neutronOpenvSwitchAgentConfig: crd.#anyconfig
										bridgeConfig: {
											type: "array"
											items: {
												type: "object"
												required: ["bridgeName", "uplinkDevice"]
												properties: {
													bridgeName: type:   "string"
													uplinkDevice: type: "string"
												}
											}
										}
										overlayNetworkConfig: {
											type: "object"
											properties: {
												ovs_local_ip_subnet: type: "string"
											}
										}
									}
									resources: {
										type: "object"
										properties: {
											"neutron-ovs-bridge-setup":  crd.#containerresources
											"neutron-openvswitch-agent": crd.#containerresources
											"ovs-vswitchd":              crd.#containerresources
											"ovsdb-server":              crd.#containerresources
										}
									}
								}
							}
							l3: {
								crd.pernodeconfig
								properties: {
									configTemplates: items: properties: {
										neutronConfig:              crd.#anyconfig
										neutronL3AgentConfig:       crd.#anyconfig
										neutronMetadataAgentConfig: crd.#anyconfig
									}
									startupLimitMinutes: {
										type:    "integer"
										default: 60
									}
									evictor: {
										type:        "object"
										description: "Configuration for the L3 eviction job"
										properties: {
											allowFallback: {
												type:        "boolean"
												default:     true
												description: "This setting allows the evict job to disable the agent if not all routers could be migrated because no agent was available for them. This violates redundancy and is unsafe"
											}
											pollInterval: {
												type:        "integer"
												default:     5
												description: "Defines in seconds how long to wait between iterates until the next poll of the API."
											}
											maxParallelMigrations: {
												type:        "integer"
												default:     15
												description: "Defines how many routers may be evacuated in parallel per iteration"
											}
											respectAvailabilityZones: {
												type:        "boolean"
												default:     false
												description: "If enabled, excludes all agents where the router is already scheduled except the availibility zone of the agent being evicted."
											}
											verifySeconds: {
												type:        "integer"
												default:     0
												description: "If the value is greater than 0, allows the user to specify how long the API should be queried until the router is active, if this did not work in the defined time period an error is logged"
											}
										}
									}
									resources: {
										type: "object"
										properties: {
											"neutron-l3-agent":       crd.#containerresources
											"neutron-metadata-agent": crd.#containerresources
											"l3-evict-job":           crd.#containerresources
										}
									}
								}
							}
							dhcp: {
								crd.pernodeconfig
								properties: {
									configTemplates: items: properties: {
										neutronConfig:              crd.#anyconfig
										neutronDHCPAgentConfig:     crd.#anyconfig
										neutronMetadataAgentConfig: crd.#anyconfig
									}
									evictor: {
										type:        "object"
										description: "Configuration for the DHCP eviction job"
										properties: {
											pollInterval: {
												type:        "integer"
												default:     5
												description: "Defines in seconds how long to wait between iterates until the next poll of the API"
											}
											maxParallelMigrations: {
												type:        "integer"
												default:     5
												description: "Defines how many networks may be evacuated in parallel per iteration"
											}
										}
									}
									resources: {
										type: "object"
										properties: {
											"neutron-dhcp-agent":     crd.#containerresources
											"neutron-metadata-agent": crd.#containerresources
											"dhcp-evict-job":         crd.#containerresources
										}
									}
								}
							}
							bgp: {
								type: "object"
								additionalProperties: {
									type: "object"
									crd.pernodeconfig
									properties: {
										configTemplates: items: properties: {
											neutronConfig:           crd.#anyconfig
											neutronBGPDRAgentConfig: crd.#anyconfig
											bgpInterfaceMapping: {
												type: "object"
												required: ["bridgeName"]
												properties: {
													bridgeName: type: "string"
												}
											}
										}
										resources: {
											type: "object"
											properties: {
												"neutron-bgp-interface-setup": crd.#containerresources
												"neutron-bgp-dragent":         crd.#containerresources
												"bgp-evict-job":               crd.#containerresources
											}
										}
									}
								}
							}
						}
					}
				}
			}

			api: {
				description: "Neutron API deployment configuration"
				properties: resources: {
					type: "object"
					properties: {
						"neutron-api":             crd.#containerresources
						"ssl-terminator":          crd.#containerresources
						"ssl-terminator-external": crd.#containerresources
						"ssl-terminator-internal": crd.#containerresources
						"service-reload":          crd.#containerresources
						"service-reload-external": crd.#containerresources
						"service-reload-internal": crd.#containerresources
					}
				}
			}

			jobResources: {
				type:        "object"
				description: "Resource limits for Job Pod containers spawned by the Operator"
				properties: {
					"neutron-db-sync-job": crd.#containerresources
				}
			}
		}
	}
}
