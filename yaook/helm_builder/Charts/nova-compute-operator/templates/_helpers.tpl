{{- define "yaook.operator.name" -}}
nova_compute
{{- end -}}

{{- define "yaook.operator.extraEnv" -}}
- name: YAOOK_NOVA_COMPUTE_OP_JOB_IMAGE
  value: {{ include "yaook.operator-lib.image" . }}
{{- end -}}
