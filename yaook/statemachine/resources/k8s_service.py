#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Kubernetes Service resources
############################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: Service

.. autoclass:: Ingress

.. autoclass:: TemplatedService

.. autoclass:: TemplatedIngress
"""
import typing

import kubernetes_asyncio.client as kclient

from .. import api_utils, interfaces
from .k8s import BodyTemplateMixin, SingleObject


class Service(SingleObject[kclient.V1Service]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[
                kclient.V1Service]:
        return interfaces.service_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1Service,
                      new: typing.Mapping) -> bool:

        cyaml = api_utils.k8s_obj_to_yaml_data(current)["spec"]
        if "type" in cyaml and cyaml["type"] != new.get("spec", {}).get(
            "type", "ClusterIP"
        ):
            return True
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml.get("ports"),
                                           new.get("spec", {}).get("ports")) or
                api_utils.deep_has_changes(cyaml.get("selector"),
                                           new.get("spec", {}).get("selector"))
                )


class Ingress(SingleObject[kclient.NetworkingV1Api]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[
                kclient.NetworkingV1Api]:
        return interfaces.ingress_interface(api_client)

    def _needs_update(self,
                      current: kclient.
                      NetworkingV1Api,
                      new: typing.Mapping) -> bool:
        cyaml = api_utils.k8s_obj_to_yaml_data(current)
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml["spec"], new["spec"]))


class TemplatedService(BodyTemplateMixin, Service):
    """
    Manage a jinja2-templated Service.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
    """


class TemplatedIngress(BodyTemplateMixin, Ingress):
    """
    Manage a jinja2-templated Deployment.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.IngressState`:
            for arguments specific to Ingresses.
    """
