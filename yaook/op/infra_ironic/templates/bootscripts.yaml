##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set imgserv_url = "http://%s:%s" | format(crd_spec.imageServer.ingress.fqdn, crd_spec.imageServer.ingress.port) %}
{% set api_url = "https://%s:%s" | format(crd_spec.api.ingress.fqdn, crd_spec.api.ingress.port) %}
{% set inspector_api_url = "https://%s:%s" | format(crd_spec.inspectorApi.ingress.fqdn, crd_spec.inspectorApi.ingress.port) %}
apiVersion: v1
kind: ConfigMap
metadata:
  generateName: {{ "%s-bootscripts-" | format(labels['state.yaook.cloud/parent-name']) }}
data:
  boot.ipxe: |
    #!ipxe

    isset ${mac:hexhyp} && goto boot_system ||
    chain ipxe.pxe

    # load the MAC-specific file or fail if it's not found. This is not mandatory although ironic likes that approach
    # it also works if a node runs into blank ipa and just fetches the steps from the conductor
    :boot_system
    chain {{ "{{" | unsafe }} ipxe_for_mac_uri {{ "}}" | unsafe }}${mac:hexhyp} || goto check_arch

    :check_arch
    set arch ${buildarch}
    iseq ${arch} x86_64 && goto inspector_ipa_x86 ||
    iseq ${arch} arm64 && goto inspector_ipa_arm64

    :inspector_ipa_x86
    chain {{ "{{" | unsafe }} ipxe_for_mac_uri {{ "}}" | unsafe }}default-x86 || goto error_no_config

    :inspector_ipa_arm64
    chain {{ "{{" | unsafe }} ipxe_for_mac_uri {{ "}}" | unsafe }}default-arm64 || goto error_no_config

    :error_no_config
    echo PXE boot failed. No configuration found for MAC ${mac}
    echo Press any key to reboot...
    prompt --timeout 180 || reboot
    reboot
  default-x86.ipxe: |
    #!ipxe

    goto introspect

    :introspect
    kernel {{ imgserv_url | unsafe }}/ipa.kernel ipa-inspection-callback-url={{ inspector_api_url | unsafe }}/v1/continue ipa-api-url={{ api_url | unsafe }} BOOTIF=${mac} quiet nofb nomodeset vga=normal ipa-insecure=1 {{ (crd_spec.ipa | default({})).pxeAppendKernelParams | default("") | unsafe }} initrd=ipa.initramfs
    initrd {{ imgserv_url | unsafe }}/ipa.initramfs
    boot

  default-arm64.ipxe: |
    #!ipxe

    goto introspect

    :introspect
    kernel {{ imgserv_url | unsafe }}/ipa_arm64.kernel ipa-inspection-callback-url={{ inspector_api_url | unsafe }}/v1/continue ipa-api-url={{ api_url | unsafe }} BOOTIF=${mac} quiet nofb nomodeset vga=normal ipa-insecure=1 {{ (crd_spec.ipa | default({})).pxeAppendKernelParams | default("") | unsafe }} initrd=ipa_arm64.initramfs
    initrd {{ imgserv_url | unsafe }}/ipa_arm64.initramfs
    boot
