##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "placement"
  labels:
    app: "placement"
spec:
  replicas:  {{ crd_spec.placement.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
{% if target_release in params.integrated_placement_releases %}
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
{% else %}
        config-timestamp: {{ dependencies['placement_config'].last_update_timestamp() }}
{% endif %}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "placement"
          image: {{ versioned_dependencies['placement_docker_image'] }}
          imagePullPolicy: Always
          volumeMounts:
{% if target_release in params.integrated_placement_releases %}
            - name: nova-config-volume
              mountPath: "/etc/nova/nova.conf"
              subPath: nova.conf
{% else %}
            - name: placement-config-volume
              mountPath: "/etc/placement"
{% endif %}
            - name: ca-certs
              mountPath: /etc/ssl/certs
            - name: tls-secret
              mountPath: /data
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
{% if crd_spec.api.wsgiProcesses | default(False) %}
            - name: WSGI_PROCESSES
              value: {{ "%s" | format(crd_spec.api.wsgiProcesses) }}
{% endif %}
          livenessProbe:
            exec:
              command:
                - curl
                - localhost:8080
          readinessProbe:
            exec:
              command:
                - curl
                - localhost:8080
          startupProbe:
            exec:
              command:
                - curl
                - localhost:8080
            periodSeconds: 10
            failureThreshold: 30
          resources: {{ crd_spec | resources('placement.placement') }}
        - name: "ssl-terminator"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "8778"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9090"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8778
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8778
              scheme: HTTPS
          resources: {{ crd_spec | resources('placement.ssl-terminator') }}
        - name: "ssl-terminator-external"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "8779"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9091"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8779
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8779
              scheme: HTTPS
          resources: {{ crd_spec | resources('placement.ssl-terminator-external') }}
{% if crd_spec.placement.internal | default(False) %}
        - name: "ssl-terminator-internal"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "8780"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9092"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8780
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8780
              scheme: HTTPS
          resources: {{ crd_spec | resources('placement.ssl-terminator-internal') }}
{% endif %}
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('placement.service-reload') }}
        - name: "service-reload-external"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('placement.service-reload-external') }}
{% if crd_spec.placement.internal | default(False) %}
        - name: "service-reload-internal"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('placement.service-reload-internal') }}
{% endif %}
      volumes:
{% if target_release in params.integrated_placement_releases %}
        - name: nova-config-volume
          secret:
            secretName: {{ dependencies['config'].resource_name() }}
            items:
              - key: nova.conf
                path: nova.conf
{% else %}
        - name: placement-config-volume
          projected:
            sources:
              - secret:
                  name: {{ dependencies['placement_config'].resource_name() }}
                  items:
                    - key: placement.conf
                      path: placement.conf
              - configMap:
                  name: {{ dependencies['ready_placement_policy'].resource_name() }}
                  items:
                    - key:  policy.yaml
                      path: policy.yaml
{% endif %}
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_placement_certificate_secret'].resource_name() }}
        - name: tls-secret-external
          secret:
            secretName: {{ dependencies['placement_external_certificate_secret'].resource_name() }}
        - name: ssl-terminator-config
          emptyDir: {}
        - name: ssl-terminator-external-config
          emptyDir: {}
{% if crd_spec.placement.internal | default(False) %}
        - name: tls-secret-internal
          secret:
            secretName: {{ dependencies['placement_internal_certificate_secret'].resource_name() }}
        - name: ssl-terminator-internal-config
          emptyDir: {}
{% endif %}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
