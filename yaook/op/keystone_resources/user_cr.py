#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import environ
import typing

import kubernetes_asyncio.client as kclient

import yaook.op.common
import yaook.statemachine as sm

from . import osresources
from .common import OpKeystoneConfig


class AutoGeneratedUserCredentials(sm.Secret):
    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> sm.ResourceBody:
        username = osresources.create_qualified_username(
            ctx.parent_name, ctx.namespace)
        return {
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": {
                "generateName": f"{ctx.parent_name}-idcreds-",
            },
            "data": sm.api_utils.encode_secret_data({
                "OS_PASSWORD": sm.resources.generate_password(32),
                "OS_USERNAME": username,
                "OS_PROJECT_NAME": "service",
                "OS_USER_DOMAIN_NAME": "Default",
                "OS_PROJECT_DOMAIN_NAME": "Default",
                "OS_AUTH_TYPE": "password",
            }),
        }

    def _needs_update(self,
                      current: kclient.V1Secret,
                      new: typing.Mapping) -> bool:
        return False


class KeystoneUser(sm.CustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "keystoneusers"
    KIND = "KeystoneUser"

    keystone = sm.KeystoneReference()

    credentials = AutoGeneratedUserCredentials(
        component=yaook.op.common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
    )

    admin_secret = sm.ForeignResourceDependency(
        resource_interface_factory=sm.secret_interface,
        foreign_resource=keystone,
        foreign_component=yaook.op.common.KEYSTONE_ADMIN_CREDENTIALS_COMPONENT,
    )

    config = sm.ForeignResourceDependency(
        resource_interface_factory=sm.config_map_interface,
        foreign_resource=keystone,
        foreign_component=yaook.op.common.KEYSTONE_INTERNAL_API_COMPONENT,
    )

    ca_config = sm.ForeignResourceDependency(
        resource_interface_factory=sm.config_map_interface,
        foreign_resource=keystone,
        foreign_component=yaook.op.common.KEYSTONE_CA_CERTIFICATES_COMPONENT,
    )

    create_user = osresources.KeystoneUser(
        admin_credentials=admin_secret,
        endpoint_config=config,
        user_credentials=credentials,
        ca_config=ca_config,
        finalizer="openstack-user.keystoneusers.yaook.cloud",
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)
        config = environ.to_config(OpKeystoneConfig)
        self.config._foreign_component = {
            "internal": yaook.op.common.KEYSTONE_INTERNAL_API_COMPONENT,
            "public": yaook.op.common.KEYSTONE_PUBLIC_API_COMPONENT,
        }[config.interface]


sm.register(KeystoneUser)
