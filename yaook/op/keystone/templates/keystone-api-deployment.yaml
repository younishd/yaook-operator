##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "keystone-api"
spec:
  replicas:  {{ crd_spec.api.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "keystone"
          image:  {{ versioned_dependencies['keystone_docker_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            # Note: we need to keep using subPath here as the image contains a `keystone-paste.ini` that we must not overwrite
            - name: keystone-config-vol
              mountPath: /etc/keystone/keystone.conf
              subPath: keystone.conf
            - name: keystone-config-vol
              mountPath: /etc/keystone/policy.yaml
              subPath: policy.yaml
            - name: keystonecredential
              mountPath: /etc/keystone/credential-keys
            - name: keystonefernet
              mountPath: /etc/keystone/fernet-keys
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
{% if crd_spec.api.wsgiProcesses | default(False) %}
            - name: WSGI_PROCESSES
              value: {{ "%s" | format(crd_spec.api.wsgiProcesses) }}
{% endif %}
          livenessProbe:
            exec:
              command:
                - curl
                - localhost:8080
          readinessProbe:
            exec:
              command:
                - curl
                - localhost:8080
          startupProbe:
            exec:
              command:
                - curl
                - localhost:8080
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 30
          resources: {{ crd_spec | resources('api.keystone') }}
        - name: "ssl-terminator"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "5000"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9090"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 5000
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 5000
              scheme: HTTPS
          resources: {{ crd_spec | resources('api.ssl-terminator') }}
        - name: "ssl-terminator-external"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "5001"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9091"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 5001
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 5001
              scheme: HTTPS
          resources: {{ crd_spec | resources('api.ssl-terminator-external') }}
{% if crd_spec.api.internal | default(False) %}
        - name: "ssl-terminator-internal"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "5002"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9092"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 5002
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 5002
              scheme: HTTPS
          resources: {{ crd_spec | resources('api.ssl-terminator') }}
{% endif %}
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload') }}
        - name: "service-reload-external"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload-external') }}
{% if crd_spec.api.internal | default(False) %}
        - name: "service-reload-internal"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload') }}
{% endif %}
      volumes:
        - name: keystone-config-vol
          projected:
            sources:
            - secret:
                name: {{ dependencies['config'].resource_name() }}
                items:
                  - key: keystone.conf
                    path: keystone.conf
            - configMap:
                name: {{ dependencies['ready_keystone_policy'].resource_name() }}
                items:
                  - key: policy.yaml
                    path: policy.yaml
        - name: keystonecredential
          secret:
            secretName: {{ dependencies['credential_keys'].resource_name() }}
        - name: keystonefernet
          secret:
            secretName: {{ dependencies['fernet_keys'].resource_name() }}
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_certificate_secret'].resource_name() }}
        - name: tls-secret-external
          secret:
            secretName: {{ dependencies['external_certificate_secret'].resource_name() }}
{% if crd_spec.api.internal | default(False) %}
        - name: tls-secret-internal
          secret:
            secretName: {{ dependencies['internal_certificate_secret'].resource_name() }}
        - name: ssl-terminator-internal-config
          emptyDir: {}
{% endif %}
        - name: ssl-terminator-config
          emptyDir: {}
        - name: ssl-terminator-external-config
          emptyDir: {}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
