#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import argparse
import types
import typing

import environ
import kubernetes_asyncio.client

import yaook.statemachine as sm

from . import eviction


@environ.config(prefix="YAOOK_NEUTRON_L3_AGENT_EVICT")
class EvictionConfig:
    node_name = environ.var()
    node_namespace = environ.var()
    reason = environ.var()
    poll_interval = environ.var(5, converter=int)
    max_parallel_migrations = environ.var(default=15, converter=int)
    respect_azs = environ.bool_var(default=False)
    verify_seconds = environ.var(default=0, converter=int)
    allow_fallback = environ.bool_var(default=True)


async def do_evacuate(args: types.SimpleNamespace) -> int:
    try:
        kubernetes_asyncio.config.load_incluster_config()
    except Exception:
        await kubernetes_asyncio.config.load_kube_config()

    config = environ.to_config(EvictionConfig)
    async with kubernetes_asyncio.client.ApiClient() as api_client:
        evictor = eviction.Evictor(
            interface=sm.neutron_l3_agent_interface(api_client),
            namespace=config.node_namespace,
            node_name=config.node_name,
            max_parallel_migrations=config.max_parallel_migrations,
            respect_azs=config.respect_azs,
            verify_seconds=config.verify_seconds,
            handle_non_migrated_routers_as_unhandleable=not config.allow_fallback,  # noqa: E501
            # do not pass any connection info, it should be sourced from the
            # environment by the OpenStack SDK :)
            connection_info={},
            reason=config.reason,
        )
        await evictor.run(poll_interval=config.poll_interval)

    return 0


async def command(argv: typing.List[str]) -> int:
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    subparser = subparsers.add_parser("evict")
    subparser.set_defaults(func=do_evacuate)

    args = parser.parse_args(argv[1:])

    return await args.func(args)
