##
## Copyright (c) 2022 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: v1
kind: ConfigMap
metadata:
  generateName: {{ "ovn-bgp-%s-" | format(labels['state.yaook.cloud/parent-name']) }}
data:
  ovn-bgp-agent.conf: |
    [DEFAULT]
    address_scopes={{ crd_spec.addressScopes | join(',') | unsafe }}
    driver={{ crd_spec.driver | unsafe }}
    bgp_router_id={{ vars.router_id | unsafe }}
    bgp_vrf={{ vars.bgp_vrf | unsafe }}
    bgp_nic={{ vars.bgp_vrf_nic | unsafe }}
    bgp_AS={{ crd_spec.localAS }}
    ovsdb_connection=unix:/run/openvswitch/db.sock
    ovn_sb_private_key=/etc/ssl/private/tls.key
    ovn_sb_certificate=/etc/ssl/private/tls.crt
    ovn_sb_ca_cert=/etc/ssl/private/ca.crt
    reconcile_interval={{ crd_spec.syncInterval }}

  bridge-interface-mapping: |
    OVS_BRIDGENAME={{ vars.ovs_bridge_name }}
    BGP_INTERFACE={{ vars.bgp_interface }}
    BGP_LOCAL_PEER_IP={{ vars.local_peer_ip }}

  frr.conf: |
    log stdout debugging
    frr version 8.1
    frr defaults traditional
    hostname {{ crd_spec.hostname | unsafe }}
    no ipv6 forwarding
    no ip forwarding
    !
    debug bgp neighbor-events
    debug bgp updates
    !
    router bgp {{ crd_spec.localAS }}
     bgp router-id {{ vars.router_id | unsafe }}
     neighbor pg peer-group
     {%- for peer in crd_spec.peers.values() %}
     neighbor {{ peer.IP | unsafe }} remote-as {{ peer.AS | unsafe }}
     neighbor {{ peer.IP | unsafe }} peer-group pg
     neighbor {{ peer.IP | unsafe }} disable-connected-check
     {%- endfor %}
     address-family ipv6 unicast
      redistribute kernel
      neighbor pg activate
      neighbor pg route-map IMPORT in
      neighbor pg route-map EXPORT out
     exit-address-family
     !
     address-family ipv4 unicast
      redistribute kernel
      neighbor pg activate
      neighbor pg route-map IMPORT in
      neighbor pg route-map EXPORT out
     exit-address-family
     !
    route-map EXPORT deny 100
    !
    route-map EXPORT permit 1
      match interface {{ vars.bgp_vrf_nic | unsafe }}
      set ip next-hop unchanged
    !
    route-map IMPORT deny 1
    !
    line vty
    !
