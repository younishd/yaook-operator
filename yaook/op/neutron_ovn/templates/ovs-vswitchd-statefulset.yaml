##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ dependencies['new_ovs_vswitchd_service'].resource_name() }}
  annotations:
    test.yaook.cloud/skip-ca-certs: "true"
spec:
  serviceName: {{ dependencies['new_ovs_vswitchd_service'].resource_name() }}
  selector:
    matchLabels: {{ labels }}
  template:
    metadata:
      labels: {{ labels }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      tolerations: {{ params["tolerations"] }}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchFields:
              - key: metadata.name
                operator: In
                values:
                # The name of the NeutronOVNAgent resource must always be the
                # name of the Kubernetes node it runs on. So we use that here
                # to schedule the pod.
                # A bit of a hack, admittedly, but covered by tests, sooo...
                - {{ labels['state.yaook.cloud/parent-name'] }}
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                state.yaook.cloud/component: ovsdb_server
            topologyKey: kubernetes.io/hostname
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      containers:
        - name: "ovs-vswitchd"
          image:  {{ versioned_dependencies['openvswitch_docker_image'] }}
          imagePullPolicy: Always
          securityContext:
            capabilities:
              add:
              - NET_ADMIN
              - NET_BROADCAST
              - NET_RAW
          command: ["/ovs-vswitchd-runner.sh"]
          volumeMounts:
            - name: run-openvswitch
              mountPath: /run/openvswitch
          lifecycle:
            preStop:
              exec:
                command:
                - sh
                - -c
                - while ss -x -a | grep /var/run/ovn/ovn-controller ; do sleep 1; done && ovs-appctl exit && while test -d /proc/1 ; do sleep 1; done
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          livenessProbe:
            exec:
              command:
              - ovs-vsctl
              - list-br
          readinessProbe:
            exec:
              command:
              - ovs-vsctl
              - list-br
          resources: {{ crd_spec | resources('ovs-vswitchd') }}
        - name: "ovs-vswitchd-monitoring"
          image:  {{ versioned_dependencies['ovsdb_monitoring_image'] }}
          imagePullPolicy: Always
          securityContext:
            capabilities:
              add:
              - NET_ADMIN
          command: [/provide_vswitchd_metrics.py]
          env:
            - name: SCRAPE_INTERVAL
              value: {{ crd_spec.scrapeIntervalMs | default(30) | string }}
            - name: PROMETHEUS_ENDPOINT_HTTP_SERVER_PORT
              value: "9999"
          ports:
            - name: metrics
              containerPort: 9999
              protocol: TCP
          volumeMounts:
            - name: run-openvswitch
              mountPath: /run/openvswitch
          resources: {{ crd_spec | resources('ovs-vswitchd-monitoring') }}
        - name: ssl-terminator
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "8008"
            - name: METRICS_PORT
              value: "8007"
            - name: LOCAL_PORT
              value: "9999"
          ports:
            - name: ovs-metrics
              containerPort: 8007
              protocol: TCP
            - name: metrics
              containerPort: 8008
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8007
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8007
              scheme: HTTPS
          resources: {{ crd_spec | resources('ssl-terminator') }}
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('service-reload') }}
      volumes:
        - name: run-openvswitch
          hostPath:
            path: /run/openvswitch
        - name: ssl-terminator-config
          emptyDir: {}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_ovs_vswitchd_certificate_secret'].resource_name() }}
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
