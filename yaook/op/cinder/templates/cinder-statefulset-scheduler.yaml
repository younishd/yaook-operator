##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: "cinder-scheduler"
  labels:
    app: "cinder-scheduler"
spec:
  podManagementPolicy: "Parallel"
  replicas:  {{ crd_spec.scheduler.replicas }}
  serviceName: "cinder-scheduler"
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "cinder-scheduler"
          image:  {{ versioned_dependencies['cinder_docker_image'] }}
          imagePullPolicy: Always
          args: ["cinder-scheduler"]
          volumeMounts:
            - name: cinder-config-volume
              mountPath: /etc/cinder/cinder.conf
              subPath: cinder.conf
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
{% if crd_spec.ids | default(False) and crd_spec.ids.uid | default(False) and crd_spec.ids.gid | default(False) %}
            - name: CINDER_UID
              value: {{ crd_spec.ids.uid | string }}
            - name: CINDER_GID
              value: {{ crd_spec.ids.gid | string }}
{% endif %}
          resources: {{ crd_spec | resources('scheduler.cinder-scheduler') }}
      volumes:
        - name: cinder-config-volume
          secret:
            secretName: {{ dependencies['config'].resource_name() }}
            items:
              - key: cinder.conf
                path: cinder.conf
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
