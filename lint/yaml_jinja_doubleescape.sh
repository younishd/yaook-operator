#!/bin/bash

ISSUE_LOCATIONS=$(find -type f -name "*.yaml" -exec grep -nH "\"{{" {} \; | grep -v "unsafe")
if [[ -n "$ISSUE_LOCATIONS" ]]; then
    echo "The following issues where found:"
    echo ""
    echo "$ISSUE_LOCATIONS"
    echo ""
    echo "Please use '|format' instead of double-escaping"
    exit 1
else
    echo "No issues found"
fi
