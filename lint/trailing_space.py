#!/usr/bin/python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import fileinput
import sys

ok = True

with fileinput.input(openhook=fileinput.hook_encoded("utf8")) as files:
    for line in files:
        # empty files are not flagged as errors, because no line is
        # yielded for them

        if line[-1] != '\n':
            print(f"{fileinput.filename()}:{fileinput.filelineno()}: "
                  "Missing newline at end of file")
            ok = False
            continue

        line = line[:-1]  # remove the trailing newline
        if len(line) - len(line.rstrip()) > 0:
            print(f"{fileinput.filename()}:{fileinput.filelineno()}: "
                  "Trailing space at end of line")
            ok = False

if not ok:
    sys.exit(1)
