Deprecation Policy
==================

Below we outline our policy on deprecating and removing features, openstack releases and cleanup jobs from the operators.

Cleanup of old resources
------------------------

From time to time we need to make adjustments to the resources a given operator provisions.
This might be as small as changing a deployment to a statefulset or as large as moving some resources out to other operators.
We make sure the operators remove the old resources that are no resources left behind.

In order to keep our codebase readable we need to remove these old resources after a given time.
Afterwards the operator will no longer recognize these resources and will not attempt to clean them up.

This time is currently defined to be *3 Month* after the release of the operator.
If you upgrade from a release older than a month to the newest release you might need to remove these resources on your own or do multiple upgrade steps.

.. note:: This does not mean that the cleanup logic is always removed after exactly three month. It might stay longer.

Removal of Features and Openstack releases
------------------------------------------

For the removal of features and openstack releases from the operators we will always give an advanced notice of at least *1 Month*.
This notice is given using the `deprecation label <https://gitlab.com/groups/yaook/-/issues?label_name%5B%5D=deprecation>`_.

If there is no objection to this removal after *1 Month* we will go ahead and remove the feature.

.. note:: In the future we also plan to announce such things on a mailing list (once we have one)
