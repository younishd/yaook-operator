Gnocchi using Ceph
==================

.. literalinclude:: gnocchi.yaml

Gnocchi using S3
==================

.. literalinclude:: gnocchi_s3.yaml
