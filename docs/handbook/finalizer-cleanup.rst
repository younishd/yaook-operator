Finalizer Cleanup
#################

The Yaook operators make use of finalizers to protect resources against
deletion before they have been cleaned up properly.

This chapter of the user manual describes the finalizers which are in use and
which manual cleanup procedures need to be taken if they have been stripped.

``infra.yaook.cloud/cleanup-amqp-user``
=======================================

**Description:**
This finalizer represents the AMQP user inside the message queue
server to which the resource relates.

**Clean up procedure:**
Manually delete the corresponding user from the AMQP
server using the admin credentials.

``infra.yaook.cloud/cleanup-mysql-user``
========================================

**Description:**
This finalizer represents the MySQL user inside the MySQL server
to which the resource relates.

**Clean up procedure:**
Manually delete the corresponding user from the MySQL
server using the admin credentials.

``openstack-endpoint.keystoneendpoints.yaook.cloud``
====================================================

**Description:**
This finalizer represents a set of OpenStack Endpoint objects in
a Keystone server.

**Clean up procedure:**
Manually delete the OpenStack Endpoint objects from the
Keystone server using the admin credentials.

``openstack-user.keystoneusers.yaook.cloud``
============================================

**Description:**
This finalizer represents an OpenStack User in a Keystone server.

**Clean up procedure:**
Manually delete the OpenStack User object from the Keystone
server using the admin credentials.

``compute.yaook.cloud/nova-compute-node``
=========================================

**Description:**
This finalizer represents the entire customer VM state on the
node.

**Clean up procedure:**
Enter the node and ensure that no remaining qemu processes
are running on the node. The processes are configured to intentionally escape
the Kubernetes process management in order to allow restarts of libvirt and
other services without interruption of the data plane. Also ensure that all
other resources (network ports, firewall rules, local storage) have been
cleaned up correctly.

``compute.yaook.cloud/nova-hostaggregate``
==========================================

**Description:**
This finalizer represents a Nova hostaggregate within Nova-API.

**Clean up procedure:**
Manually remove all Hypervisors still present with the
aggregate (this can cause also failures in delete).
After that remove the hostaggregate manually
from Nova API using the admin credentials.
