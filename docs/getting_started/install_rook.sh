##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -e -x

#Set global variables. Feel free to adjust them to your needs
export NAMESPACE=$YAOOK_OP_NAMESPACE

# Deploy helm charts
helm repo add rook-release https://charts.rook.io/release
helm repo update

helm upgrade --install --namespace $NAMESPACE \
  --version v1.4.9 \
  rook-ceph rook-release/rook-ceph

# Deploy the ceph toolbox...
kubectl -n $NAMESPACE apply -f docs/getting_started/ceph-toolbox.yaml

# ...and the other ceph resources.
kubectl -n $NAMESPACE apply -f docs/examples/rook-cluster.yaml
kubectl -n $NAMESPACE apply -f docs/examples/rook-resources.yaml
kubectl -n $NAMESPACE apply -f docs/getting_started/rook-ceph-block.yaml
