Operator Helm chart values
##########################

This section describes the Helm chart values for the operator Helm charts added
by Yaook. The list of values might not be exhaustive. Further information about
available Helm chart values can be found in
``yaook/helm_builder/Charts/*/values-template.yaml.j2``.

``operator.dockerConfigSecret``
===============================

Available to all OpenStack service operators. Allows to pass image registry
credentials to the operators.

The operators might contact the image registry to retrieve all available tags
for an image. In case of a private registry which requires credentials,
``operator.dockerConfigSecret`` allows to specify the name of a K8s secret
which contains the credentials and is then made available to the operators.

Such a K8s secret of type ``kubernetes.io/dockerconfigjson`` can be created
like that:

.. code-block::

    kubectl -n NAMESPACE create secret docker-registry SECRETNAME --docker-password='PASSWORD' --docker-server='REGISTRYNAME.DOMAIN:PORT' --docker-username='USERNAME'

If the operator image is to be pulled from a private registry as well, you may
need to set the Helm chart value ``operator.imagePullSecrets[].name`` to the
name of a corresponding Kubernetes secret as well. For example as parameter
to helm: ``--set "operator.imagePullSecrets[0].name=SECRETNAME".``

``operator.caConfigMap``
========================

Available to all OpenStack service operators. Allows to pass a custom CA bundle
to the operators.

Set the Helm chart value ``operator.caConfigMap`` to a name of a proper
Kubernetes ConfigMap. That ConfigMap needs to have the following structure:

.. code-block::

    apiVersion: v1
    data:
      ca-bundle.crt: |-
        -----BEGIN CERTIFICATE-----
        8<... 1st certificate ...>8
        -----END CERTIFICATE-----
        -----BEGIN CERTIFICATE-----
        8<... 2nd certificate ...>8
        -----END CERTIFICATE-----
        ...
        -----BEGIN CERTIFICATE-----
        8<... n-th certificate ...>8
        -----END CERTIFICATE-----
    immutable: true
    kind: ConfigMap
    metadata:
      name: MY-CONFIGMAP-NAME

The ConfigMap then is mounted inside the operator container under
``/etc/pki/tls/certs`` and the environment variable ``REQUESTS_CA_BUNDLE`` is
set to point to ``/etc/pki/tls/certs/ca-bundle.crt``. Hence the first key of
data **MUST** be ``ca-bundle.crt``.

This is useful for example if an alternative image registry is used which is
protected by a TLS certificate not signed by a official CA.

**Note:** The operator will only use the provided CA bundle and ignore the
bundle file carried with certifi that bundles a collection of official CA
certificates. It might be desired to rather use that bundle appended by the CA
certificate of e.g. the alternative image registry. To build such a Kubernetes
ConfigMap the Python script ``./tools/create_ca_bundle_configmap.py`` can help.
It is used like that:

.. code-block::

    python create_ca_bundle_configmap.py CONFIG_MAP_NAME ./ca-certificate-of-private-registry.crt  | kubectl -n NAMESPACE apply -f -
