Helm Charts
===========

`Helm <https://helm.sh>`_ is like a package manager for applications running on Kubernetes. With Yaook Operator and the supporting services being an application running on Kubernetes, it only makes sense to offer it as a Helm chart to deploy into a cluster.

.. note::

    The official Yaook Helm Chart registry is at `<https://charts.yaook.cloud/operator/stable/>`_.

Helm Chart generation
---------------------

A working Yaook cluster depends on three pieces of generated-from-source information in order to function:

* The CustomResourceDefinitions are built using cuelang to deduplicate shared bits in them
* The Roles and ClusterRoles for operators are generated from the source code based on the privileges needed to run it.
* The versions of the images used in all components.

That means that static Helm charts, where all manifest templates are contained within a ``templates`` directory, are not a solution.

Helm Chart builder
------------------

The Helm Chart builder is located in ``yaook/helm_builder``. The chart templates are located in ``yaook/helm_builder/Charts``. That directory also contains a README file with an overview of the structure of a Chart template.

Using the Helm Chart builder
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::

    This documentation should be moved into a user guide; it is not an implementation detail.

You can run the Helm Chart builder using:

.. code-block:: console

    python -m yaook.helm_builder -vvv -a

This will build all charts. To also upload charts to the repository, add `-U` to the command line flags and set the ``GITLAB_USERNAME``, ``CI_JOB_TOKEN`` and ``CI_PROJECT_ID`` environment variables.

To only build a subset of charts, remove the ``-a`` and name the charts explicitly (with either their directory name or an absolute path to the chart template directory).

Creating a new Helm Chart
-------------------------

* Create a folder in ``/yaook/helm_builder/Charts``. This will be your Chart template folder (not to be confused with the standard ``templates`` folder within that Chart template folder). The name of the folder will become the name of your helm chart.

* Create a subfolder ``templates``. Place your Helm manifest templates and partials in that folder.

* Create ``chart-template.yaml.j2``. This is a Jinja2 Template which will be rendered by the Helm builder to create the ``Chart.yaml`` for the Chart. See the README in the ``Charts`` directory for details.

* Create ``values-template.yaml.j2``. This is a Jinja2 Template which will be rendered by the Helm builder to create the ``values.yaml`` for the Chart. See the README in the ``Charts`` directory for details.

* Test building your chart using:

    .. code-block:: console

        $ python3 -m yaook.helm_builder -vvv your-chart-name


    Check the contents of the generated ``Chart.yaml`` and ``values.yaml``. You may also test deploying your Chart using ``helm install ... releasename ./yaook/helm_builder/Charts/your-chart-name``, optionally with ``--dry-run``.

You can check out the existing charts for an example.
