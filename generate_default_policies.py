#!/usr/bin/env python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import pathlib
import subprocess  # nosemgrep

POLICY_NAMESPACES = [
    "barbican",
    "cinder",
    "glance",
    "neutron",
    "nova",
    "keystone",
]


def _writefile(modulename: str):
    base_path = pathlib.Path(f"yaook/op/{modulename}/generated")
    base_path.mkdir(parents=True, exist_ok=True)
    result = subprocess.run(  # nosemgrep
        [
            "oslopolicy-policy-generator",
            "--namespace", modulename,
            "--output-file", base_path / "default_policies.yaml"
        ],
        capture_output=True,
    )
    if result.returncode:
        msg = f"Failed to generate the default policy file for {modulename}. "
        msg += f"stdout: {result.stdout}"
        msg += f"stderr: {result.stderr}"
        raise Exception(msg)


def generate_policy_file(modulename: str):
    _writefile(modulename)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        for module in POLICY_NAMESPACES:
            # This is to work around oslo config that does not expect to have
            # multiple overlapping configs (e.g. glance and nova) in the same
            # python process. Without this the same config key is defined
            # multiple times (from an oslo.config perspecitve)
            subprocess.run(  # nosemgrep
                [sys.executable, "generate_default_policies.py", module]
            )
    else:
        print("Generating default policy file for: %s" % sys.argv[1])
        generate_policy_file(sys.argv[1])
